package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Creatures.*;
import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Prototype;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * The Room class represents a room within a floor of a house, managing devices and creatures present.
 */
public class Room implements Component, Prototype {
    public static int DEFAULT_AREA = 9;
    private Floor floor;
    private final String name;
    private final int area;
    private final List<Device> devices;
    private final List<Creature> creatures;
    /**
     * Constructs a room with the given name and assigns it to the specified floor.
     *
     * @param floor The floor to which the room belongs.
     * @param name  The name of the room.
     */
    public Room(Floor floor,String name) {
        this.floor=floor;
        this.name = name;
        area = DEFAULT_AREA;
        devices = new ArrayList<Device>();
        creatures= new ArrayList<Creature>();
    }
    /**
     * Constructs a room with the given name, area, and assigns it to the specified floor.
     *
     * @param floor The floor to which the room belongs.
     * @param name  The name of the room.
     * @param area  The area of the room in square units.
     */
    public Room(Floor floor,String name, int area) {
        this.floor=floor;
        this.name = name;
        this.area = area;
        devices = new ArrayList<Device>();
        creatures= new ArrayList<Creature>();
    }

    public String getName() {
        return name;
    }
    /**
     * Adds a device to the room's device list and sets its room reference to this room.
     *
     * @param dev The device to be added to the room.
     */
    public void addDevice(Device dev) {
        devices.add(dev);
        dev.setRoom(this);
    }
    public List<Device> getDevices() {
        return devices;
    }
    public List<Creature> getCreatures() {
        return creatures;
    }
    /**
     * Adds a creature to the room's creature list if it's not already present and sets its room reference to this room.
     *
     * @param creature The creature to be added to the room.
     */
    public void addCreature(Creature creature){
        if(!this.getFloor().getHouse().getFloors().isEmpty()) {
            List<Creature> creaturesList = this.getFloor().getHouse().getFloors().stream()
                    .flatMap(floor -> floor.getRooms().stream())
                    .flatMap(room -> room.getCreatures().stream())
                    .collect(Collectors.toList());
            boolean flag= false;
            for (Creature cr:creaturesList) {
                if(creature instanceof Mother && cr instanceof Mother){
                    flag=true;
                    break;
                }
                if(creature instanceof Father && cr instanceof Father){
                    flag=true;
                    break;
                }
                if(creature instanceof Child && cr instanceof Child){
                    flag=true;
                    break;
                }
                if(creature instanceof Dog && cr instanceof Dog){
                    flag=true;
                    break;
                }
            }
            if(!flag){
                creatures.add(creature);
                creature.setRoom(this);
            }

        }
    }
    public void removeCreature(Creature creature){
        creatures.remove(creature);
        creature.setRoom(null);
    }
    public Floor getFloor() {
        return floor;
    }
    public void setFloor(Floor f) {
        floor = f;
    }
    public int getArea() {
        return area;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalConsumption() {
        long consumption=0;
        for(Device dev : devices){
            consumption+=dev.getConsumedEnergy();
        }
        return consumption;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalNumberOfCreatures() {
        return creatures.size();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalNumberOfDevices() {
        return devices.size();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Room clone() {
        Room newRoom = new Room(this.floor,this.name, this.area);
        for (Device dev: this.devices) {
            newRoom.addDevice(dev.clone());
        }
        return newRoom;
    }
}
