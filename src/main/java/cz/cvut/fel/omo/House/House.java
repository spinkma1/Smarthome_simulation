package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventHandler;
import cz.cvut.fel.omo.Prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a house in the smart home system.
 * It implements the Component and Prototype interfaces.
 */
public class House implements Component, Prototype {

    private SmartHome smartHome;
    private String address;
    private List<Floor> floors;
    /**
     * Constructs a House object with an empty list of floors.
     */
    public House () {
        floors = new ArrayList<>();
    }
    public void addFloor() {
        floors.add(new Floor(this,floors.size()+1));
    }
    /**
     * Adds a room with the given name to the specified floor level in the house.
     *
     * @param floorLevel The level of the floor where the room needs to be added.
     * @param name       The name of the room to be added.
     */
    public void addRoom(int floorLevel,String name) {
        for (Floor floor:floors) {
            if(floor.getLevel()==floorLevel){
                floor.addRoom(name);
            }
        }
    }
    /**
     * Adds a room with the given name and area to the specified floor level in the house.
     *
     * @param floorLevel The level of the floor where the room needs to be added.
     * @param name       The name of the room to be added.
     * @param area       The area of the room to be added.
     */
    public void addRoom(int floorLevel,String name, int area) {
        for (Floor floor:floors) {
            if(floor.getLevel()==floorLevel){
                floor.addRoom(name, area);
            }
        }
    }
    /**
     * Installs the given device in the specified room of the house.
     * If the device implements EventHandler, it is added to the EventBroker for handling events.
     *
     * @param device The device to be installed in the room.
     * @param room   The name of the room where the device needs to be installed.
     */
    public void installDevice(Device device, String room) {
        if (device instanceof EventHandler) {
            EventBroker.getInstance().addToEventHandlers((EventHandler) device);
        }
        for (Floor f: floors) {
                for (Room r: f.getRooms()) {
                    if (r.getName()== room) {
                        r.addDevice(device);
                        return;
                    }
                }
        }
    }
    /**
     * Retrieves the list of floors within the house.
     *
     * @return The list of floors in the house.
     */
    public List<Floor> getFloors() {
        return floors;
    }
    public SmartHome getSmartHome() {
        return smartHome;
    }
    public String getAddress(){
        return address;
    }

    public void setSmartHome(SmartHome smartHome) {
        this.smartHome = smartHome;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalConsumption() {
        long consumption=0;
        for(Floor f : floors){
            consumption+=f.getTotalConsumption();
        }
        return consumption;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalNumberOfCreatures() {
        long sum=0;
        for(Floor f: floors){
            sum+=f.getTotalNumberOfCreatures();
        }
        return sum;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalNumberOfDevices() {
        long sum=0;
        for(Floor f: floors){
            sum+=f.getTotalNumberOfDevices();
        }
        return sum;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public House clone() {
        House newHouse = new House();
        newHouse.smartHome = this.smartHome;
        for (Floor f: this.floors) {
            Floor cloned = f.clone();
            cloned.setHouse(newHouse);
            newHouse.floors.add(cloned);
        }
        newHouse.address = this.address;
        return newHouse;
    }
}
