package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Devices.Device;

public class HouseBuilder {

    private House h;

    public HouseBuilder() {}
    /**
     * Retrieves the prototype of a house and sets it as the current house in the HouseBuilder.
     *
     * @return The HouseBuilder instance with the prototype house set.
     */
    public HouseBuilder getHousePrototype () {
        h = HousePrototype.getHousePrototype();
        return this;
    }

    public HouseBuilder getEmptyHouse() {
        h = new House();
        return this;
    }
    public House build() {
        if (h==null) {
            h = HousePrototype.getHousePrototype();
        }
        return h;
    }
    public HouseBuilder addFloor() {
        if (h ==null) {
            getEmptyHouse();
        }
        h.addFloor();
        return this;
    }
    public HouseBuilder addRoom(int floorLevel, String nameOfRoom) {
        if (h == null) {
            getEmptyHouse();
        }
        h.addRoom(floorLevel, nameOfRoom);
        return this;
    }
    public HouseBuilder addDevice(Device device, String nameOfRoom) {
        if (h== null) {
            getEmptyHouse();
        }
        h.installDevice(device, nameOfRoom);
        return this;
    }
}
