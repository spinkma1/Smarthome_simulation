package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Devices.Blind;
import cz.cvut.fel.omo.Devices.LightBulb;
import cz.cvut.fel.omo.Devices.Sensors.SmokeSensor;

/**
 * HousePrototype class responsible for generating a prototype instance of a house.
 * This prototype includes predefined rooms and devices for the house.
 */
public class HousePrototype {

    private static House housePrototype;

    /**
     * Retrieves a prototype instance of a house, creating it if it hasn't been initialized.
     * Populates the house with predefined rooms and devices.
     *
     * @return A prototype instance of a house with predefined rooms and devices.
     */
    public static House getHousePrototype() {
        if (housePrototype == null) {
            housePrototype = new House();
            housePrototype.addFloor();
            housePrototype.addRoom(1, "Living room", 20);
            housePrototype.addRoom(1, "Kitchen");
            housePrototype.addRoom(1, "Bathroom", 5);
            housePrototype.addRoom(1, "Bedroom");

            housePrototype.installDevice(new SmokeSensor("Smokey 3000", 50), "Kitchen");

            Blind b1 = new Blind("");
            b1.reactToWeather();
            Blind b2 = b1.clone();
            b2.reactToWeather();

            housePrototype.installDevice(b1, "Living room");
            housePrototype.installDevice(b2, "Bedroom");
            housePrototype.installDevice(new LightBulb("LG SmartLight", 60, true), "Bathroom");
        }
        return housePrototype.clone();
    }
}

