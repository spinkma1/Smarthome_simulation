package cz.cvut.fel.omo.House;

/**
 * Interface representing a component within the system.
 * It defines methods to retrieve total consumption, total number of creatures, and total number of devices.
 */
public interface Component {

    /**
     * Retrieves the total consumption of the component.
     *
     * @return The total consumption of the component.
     */
    long getTotalConsumption();

    /**
     * Retrieves the total number of creatures in the component.
     *
     * @return The total number of creatures in the component.
     */
    long getTotalNumberOfCreatures();

    /**
     * Retrieves the total number of devices in the component.
     *
     * @return The total number of devices in the component.
     */
    long getTotalNumberOfDevices();
}
