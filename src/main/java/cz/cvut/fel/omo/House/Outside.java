package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Creatures.Creature;

import java.util.ArrayList;
import java.util.List;

/**
 * The Outside class represents the area outside the smart home, managing creatures present there.
 */
public class Outside {
    private List<Creature> creaturesOutside;
    private SmartHome smartHome;

    /**
     * Constructs an instance of Outside with a reference to the associated SmartHome.
     *
     * @param smartHome The SmartHome associated with this Outside area.
     */
    public Outside(SmartHome smartHome) {
        creaturesOutside = new ArrayList<>();
        this.smartHome = smartHome;
    }

    /**
     * Retrieves the list of creatures present outside.
     *
     * @return List of creatures present outside.
     */
    public List<Creature> getCreaturesOutside() {
        return creaturesOutside;
    }

    /**
     * Retrieves the associated SmartHome.
     *
     * @return The SmartHome associated with this Outside area.
     */
    public SmartHome getSmartHome() {
        return smartHome;
    }

    /**
     * Adds a creature to the list of creatures outside and sets its outside reference to this area.
     *
     * @param creature The creature to be added outside.
     */
    public void addCreature(Creature creature) {
        creaturesOutside.add(creature);
        creature.setOutside(this);
    }

    /**
     * Removes a creature from the list of creatures outside and sets its outside reference to null.
     *
     * @param creature The creature to be removed from outside.
     */
    public void removeCreature(Creature creature) {
        creaturesOutside.remove(creature);
        creature.setOutside(null);
    }
}

