package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Prototype;

import java.util.ArrayList;
import java.util.List;

public class Floor implements Component, Prototype {
    private House house;
    private List<Room> rooms;

    private int level;

    public int getLevel() {
        return level;
    }

    public Floor(House house,int level) {
        this.house=house;
        this.level = level;
        rooms = new ArrayList<Room>();
    }
    public void addRoom(String name){
        rooms.add(new Room(this,name));
    }

    public void addRoom(String name, int area){
        rooms.add(new Room(this,name, area));
    }

    public List<Room> getRooms() {
        return rooms;
    }
    public House getHouse() {
        return house;
    }
    public void setHouse(House h) {
        house =h;
    }

    @Override
    public long getTotalConsumption() {
        long consumption=0;
        for(Room room : rooms){
            consumption+=room.getTotalConsumption();
        }
        return consumption;
    }

    @Override
    public long getTotalNumberOfCreatures() {
        long sum=0;
        for(Room room: rooms){
            sum+=room.getTotalNumberOfCreatures();
        }
        return sum;
    }
    @Override
    public long getTotalNumberOfDevices() {
        long sum=0;
        for(Room room: rooms){
            sum+=room.getTotalNumberOfDevices();
        }
        return sum;
    }
    @Override
    public Floor clone() {
        Floor newFloor = new Floor(this.house,this.level);
        for (Room r : this.rooms) {
            Room cloned = r.clone();
            cloned.setFloor(newFloor);
            newFloor.rooms.add(cloned);
        }
        return newFloor;
    }
}
