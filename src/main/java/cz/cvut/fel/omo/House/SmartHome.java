package cz.cvut.fel.omo.House;

import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Reports.ActivityReport;
import cz.cvut.fel.omo.Reports.ConsumptionReport;
import cz.cvut.fel.omo.Reports.EventReport;
import cz.cvut.fel.omo.Reports.HouseConfigurationReport;

import java.io.IOException;


/**
 * SmartHome class represents a central entity managing the house, outside area, and various reports.
 */
public class SmartHome {

    private House house;
    private Outside outside;
    private HouseConfigurationReport houseConfigurationReport;
    private ActivityReport activityReport;
    private EventReport eventReport;
    private ConsumptionReport consumptionReport;

    /**
     * Initializes a new SmartHome instance.
     * Initializes the reports and connects them to this SmartHome instance.
     */
    public SmartHome() {
        this.outside = new Outside(this);
        houseConfigurationReport = new HouseConfigurationReport();
        houseConfigurationReport.setSmarthome(this);
        activityReport = new ActivityReport();
        activityReport.setSmarthome(this);
        eventReport = new EventReport();
        eventReport.setSmarthome(this);
        consumptionReport = new ConsumptionReport();
        consumptionReport.setSmarthome(this);
    }

    /**
     * Retrieves the house associated with this SmartHome instance.
     *
     * @return The house object associated with the SmartHome.
     */
    public House getHouse() {
        return house;
    }

    /**
     * Sets the house for this SmartHome instance.
     * Additionally sets this SmartHome as the house's SmartHome.
     *
     * @param house The house to be associated with this SmartHome.
     */
    public void setHouse(House house) {
        this.house = house;
        house.setSmartHome(this);
    }

    /**
     * Retrieves the outside area associated with this SmartHome instance.
     *
     * @return The Outside object associated with the SmartHome.
     */
    public Outside getOutside() {
        return outside;
    }

    /**
     * Generates and retrieves the House Configuration Report.
     */
    public void getHouseConfigurationReport() throws IOException {
        houseConfigurationReport.generateReport();
    }

    /**
     * Generates and retrieves the Activity Report.
     * Additionally resets the history of activities in the EventBroker.
     */
    public void getActivityReport() throws IOException {
        activityReport.generateReport();
        EventBroker.getInstance().resetHistoryOfActivities();
    }

    /**
     * Generates and retrieves the Event Report.
     * Additionally resets the history of events in the EventBroker.
     */
    public void getEventReport() throws IOException {
        eventReport.generateReport();
        EventBroker.getInstance().resetHistoryOfEvents();
    }

    /**
     * Generates and retrieves the Consumption Report.
     */
    public void getConsumptionReport() throws IOException {
        consumptionReport.generateReport();
    }
}

