package cz.cvut.fel.omo;

/**
 * The Prototype interface declares a method for cloning objects.
 */
public interface Prototype {
    /**
     * Creates a clone of the implementing object.
     *
     * @return A cloned instance of the implementing object.
     */
    Prototype clone();
}

