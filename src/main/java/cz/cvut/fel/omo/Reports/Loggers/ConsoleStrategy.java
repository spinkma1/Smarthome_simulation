package cz.cvut.fel.omo.Reports.Loggers;

import java.util.logging.*;

/**
 * ConsoleStrategy class implements LogStrategy and logs messages to the console using Java's Logger.
 */
public class ConsoleStrategy implements LogStrategy {
    private static final Logger logger = Logger.getLogger(ConsoleStrategy.class.getName());

    /**
     * Logs the provided message to the console using the Java Logger.
     *
     * @param message The message to be logged.
     */
    @Override
    public void log(String message) {
        logger.info(message);
    }
}
