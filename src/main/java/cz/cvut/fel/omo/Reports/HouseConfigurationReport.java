package cz.cvut.fel.omo.Reports;

import cz.cvut.fel.omo.Creatures.Creature;
import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.House.Floor;
import cz.cvut.fel.omo.House.House;
import cz.cvut.fel.omo.House.Room;
import cz.cvut.fel.omo.House.SmartHome;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class HouseConfigurationReport extends AbstractSmartHomeReporter{
    private SmartHome smarthome;
    public HouseConfigurationReport() {
    }
    /**
     * Generates an activity report containing information about the creatures and devices in the Smart Home
     * and saves it to a text file.
     *
     * @throws IOException Signals that an I/O exception has occurred during file operations.
     */
    @Override
    public void generateReport() throws IOException {
        House house=smarthome.getHouse();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm");
            String timestamp = LocalDateTime.now().format(formatter);
            File file = new File("reports/HouseConfiguration","HouseConfiguration"+ timestamp+".txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write("Total number of creatures in House with address "+house.getAddress()+" is "+house.getTotalNumberOfCreatures());
            writer.newLine();
            for (Floor floor:house.getFloors()) {
                for(Room room: floor.getRooms()){
                    List<Creature> creatureList=room.getCreatures();
                    writer.write(room.getName()+" with area "+room.getArea()+" has Creatures: ");
                    writer.newLine();
                    List<Device> deviceList=room.getDevices();
                    for (Creature c:creatureList) {
                        writer.write(c.getName()+" - "+c.getNickname());
                        writer.newLine();
                    }
                    writer.newLine();
                    writer.write("Devices: ");
                    writer.newLine();
                    for (Device d:deviceList) {
                        writer.write(d.getName()+" with id "+d.getId());
                        writer.newLine();
                    }
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
    public void setSmarthome(SmartHome smarthome) {
        this.smarthome = smarthome;
    }
}
