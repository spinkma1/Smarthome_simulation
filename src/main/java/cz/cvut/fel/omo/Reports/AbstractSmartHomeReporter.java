package cz.cvut.fel.omo.Reports;

import cz.cvut.fel.omo.House.SmartHome;

import java.io.IOException;

/**
 * The AbstractSmartHomeReporter is an abstract class that defines the structure for generating reports
 * related to a Smart Home system.
 */
public abstract class AbstractSmartHomeReporter {
    /**
     * Generates a report related to the Smart Home system.
     */
    public abstract void generateReport() throws IOException;

    /**
     * Sets the SmartHome instance for which the report will be generated.
     *
     * @param smartHome The SmartHome instance to be associated with the report generation.
     */
    public abstract void setSmarthome(SmartHome smartHome);
}

