package cz.cvut.fel.omo.Reports.Loggers;

/**
 * SmartHomeLogger manages logging strategies for the Smart Home system.
 * It allows switching between different logging strategies.
 */
public class SmartHomeLogger {
    private LogStrategy logStrategy;

    /**
     * Constructs a SmartHomeLogger object with a default logging strategy (ConsoleStrategy).
     */
    public SmartHomeLogger() {
        this.logStrategy = new ConsoleStrategy();
    }

    /**
     * Sets a new logging strategy.
     *
     * @param newLogStrategy The new logging strategy to be set.
     */
    public void setLogStrategy(LogStrategy newLogStrategy) {
        this.logStrategy = newLogStrategy;
    }

    /**
     * Logs a message using the selected logging strategy.
     *
     * @param message The message to be logged.
     */
    public void log(String message) {
        logStrategy.log(message);
    }
}
