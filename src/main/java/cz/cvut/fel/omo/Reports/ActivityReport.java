package cz.cvut.fel.omo.Reports;

import cz.cvut.fel.omo.Creatures.Activity;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.SmartHome;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
/**
 * This class represents an activity report generator for the Smart Home system.
 * It generates a report containing information about the activities performed in the Smart Home.
 */
public class ActivityReport extends AbstractSmartHomeReporter{
    private SmartHome smarthome;
    public ActivityReport() {
    }

    public void setSmarthome(SmartHome smarthome) {
        this.smarthome = smarthome;
    }
    /**
     * Generates an activity report containing information about the activities performed in the Smart Home
     * and saves it to a text file.
     *
     * @throws IOException Signals that an I/O exception has occurred during file operations.
     */
    @Override
    public void generateReport() throws IOException {
        try{
            List<Activity> listActivity=EventBroker.getInstance().getHistoryOfActivities();
            String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm"));
            File file = new File("reports/ActivityReport","ActivityReport"+ timestamp+".txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Activity activity:listActivity) {
                long mins=activity.getBegin().until(activity.getEnd(), ChronoUnit.MINUTES);
                long secs=activity.getBegin().until(activity.getEnd(), ChronoUnit.SECONDS)%60;
                writer.write(activity.getActivityMaker().getClass().getSimpleName() + ": "
                        + activity.getName() + ", "
                        + String.format("%02d", activity.getBegin().getHour()) + ":" + String.format("%02d", activity.getBegin().getMinute())
                        + " - "
                        + String.format("%02d", activity.getEnd().getHour()) + ":" + String.format("%02d", activity.getEnd().getMinute())
                        + " (" + mins + "m" + secs + "s" + ")");
                writer.newLine();
            }
            writer.close();
    } catch (IOException e) {
            throw new IOException(e);
    }
    }
}
