package cz.cvut.fel.omo.Reports;

import cz.cvut.fel.omo.Creatures.Activity;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.SmartHome;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class EventReport extends AbstractSmartHomeReporter{
    private SmartHome smarthome;
    public EventReport() {
    }

    public void setSmarthome(SmartHome smarthome) {
        this.smarthome = smarthome;
    }
    /**
     * Generates an activity report containing information about events in the Smart Home
     * and saves it to a text file.
     *
     * @throws IOException Signals that an I/O exception has occurred during file operations.
     */
    @Override
    public void generateReport() throws IOException {
        try{
            List<Event> listEvent= EventBroker.getInstance().getHistoryOfEvents();
            String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm"));
            File file = new File("reports/EventReport","EventReport"+ timestamp+".txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Event event:listEvent) {
                writer.write(event.toString());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
