package cz.cvut.fel.omo.Reports.Loggers;

/**
 * LogStrategy interface defines the contract for logging strategies.
 * Classes implementing this interface should provide a method to log a message.
 */
public interface LogStrategy {
    /**
     * Logs the provided message based on the implemented strategy.
     *
     * @param message The message to be logged.
     */
    void log(String message);
}
