package cz.cvut.fel.omo.Reports.Loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * FileStrategy class implements LogStrategy and logs messages to a file using Java's LogManager and Logger.
 */
public class FileStrategy implements LogStrategy {
    private final Logger logger;

    /**
     * Constructs a FileStrategy object with a Logger for file logging.
     */
    public FileStrategy() {
        this.logger = LogManager.getLogger(FileStrategy.class);
    }

    /**
     * Logs the provided message to a file using Java's Logger.
     *
     * @param message The message to be logged.
     */
    @Override
    public void log(String message) {
        logger.info(message);
    }
}
