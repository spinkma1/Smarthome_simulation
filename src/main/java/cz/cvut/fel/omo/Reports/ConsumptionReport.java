package cz.cvut.fel.omo.Reports;

import cz.cvut.fel.omo.Creatures.Activity;
import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.SmartHome;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class ConsumptionReport extends AbstractSmartHomeReporter{
    private SmartHome smarthome;
    public ConsumptionReport() {
    }
    /**
     * Generates an activity report containing information about the consumption in the Smart Home
     * and saves it to a text file.
     *
     * @throws IOException Signals that an I/O exception has occurred during file operations.
     */
    @Override
    public void generateReport() throws IOException {
        List<Event> eventList=EventBroker.getInstance().getHistoryOfEvents();
        List<Device> allDevices = smarthome.getHouse().getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getDevices().stream())
                .collect(Collectors.toList());


        try{
            String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm"));
            File file = new File("reports/ConsumptionReport","ConsumptionReport"+ timestamp+".txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            for (Device device:allDevices) {
                double num= device.getConsumedEnergy();
                writer.write(device.toString() + " - "
                        + String.format("%.20f", num));
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            throw new IOException(e);
        }


    }
    public SmartHome getSmarthome() {
        return smarthome;
    }

    public void setSmarthome(SmartHome smarthome) {
        this.smarthome = smarthome;
    }
}
