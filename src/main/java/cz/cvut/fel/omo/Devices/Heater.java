package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Sensors.WeatherSensor;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventHandler;

public class Heater extends AbstractSmartDevice implements EventHandler {
    private boolean handleWeather;
    private int temperatureToHeatAt;

    public Heater(String name, int consumption) {
        super(name, consumption);
        handleWeather = false;
        temperatureToHeatAt = 15;
    }

    @Override
    public Heater clone() {
        return new Heater(this.getName(), this.getConsumption());
    }

    @Override
    public void handleEvent(Event e) {
        if (!handleWeather) {
            return;
        }
        if (e.getSource() instanceof WeatherSensor) {
            String temp = e.getMessage();
            int indexOfTemp = temp.indexOf('°');
            temp = temp.substring(indexOfTemp-3, indexOfTemp);
            if (temp.charAt(0) !='-') {
                temp = temp.substring(1);
            }
            if (temp.charAt(0) == ' ') {
                temp = temp.substring(1);
            }
            int temperature = Integer.parseInt(temp);
            //System.out.println(temperature);
            if (temperature <= temperatureToHeatAt) {
                turnOn();
            } else {
                turnOff();
            }
        }
    }
    public void handleWeather() {
        handleWeather = true;
    }
    public void doNotHandleWeather() {
        handleWeather = false;
    }
    public void setTemperatureToHeatAt(int temperature) {
        temperatureToHeatAt = temperature;
    }

    public int getTemperatureToHeatAt() {
        return temperatureToHeatAt;
    }
}
