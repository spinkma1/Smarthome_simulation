package cz.cvut.fel.omo.Devices.Utils;

import java.time.LocalDate;

public class Food {
    private String name;
    private LocalDate expiryDate;

    public Food(String name, LocalDate expiryDate) {
        this.name = name;
        this.expiryDate = expiryDate;
    }
    public boolean isExperired() {
        if (expiryDate.compareTo(LocalDate.now())>0) {
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }
}
