package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Events.Event;
import java.util.Timer;
import java.util.TimerTask;

public class Oven extends AbstractSmartDevice {
    private int temperature;
    private Timer timer;


    public Oven(String name, int consumption) {
        super(name, consumption);
        temperature = 0;
    }

    @Override
    public Device clone() {
        return new Oven(this.getName(), this.getConsumption());
    }

    public void preHeat(int temperature) {
        beginFunction();
        int time = (temperature - this.temperature)*100;
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                finishPreheat(temperature);
            }
        };
        timer.schedule(task, time);
    }
    private void finishPreheat(int temperature) {
        this.temperature = temperature;
        raiseEvent(new Event(this, this.getRoom(), " is preheated at " +temperature + "°C"));
    }
    public void bake(int degrees, int minutes) {
        beginFunction();
        int time = minutes*100;
        if (temperature< degrees) {
            this.preHeat(degrees);
            time+= (degrees-temperature)*100;
        }
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                finishBaking();
            }
        };
        timer.schedule(task, time);
    }

    private void finishBaking() {
        raiseEvent(new Event(this, this.getRoom(), " finished baking"));
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                temperature=20;
                notifyCooldown();
                turnOff();
            }
        };
        timer.schedule(t, (temperature-20)* 100L);
    }

    private void notifyCooldown() {
        raiseEvent(new Event(this, this.getRoom(), " cooled down"));
    }
    @Override
    public void turnOff() {
        super.turnOff();
        timer.cancel();
    }
    @Override
    public void turnOn() {
        super.turnOn();
        timer = new Timer();
    }

    public int getTemperature() {
        return temperature;
    }
}
