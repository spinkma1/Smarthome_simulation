package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Utils.State;
import cz.cvut.fel.omo.Events.Event;

import java.util.Timer;
import java.util.TimerTask;

public class WashingMachine extends AbstractSmartDevice{

    public WashingMachine(String name, int consumption) {
        super(name, consumption);
    }

    @Override
    public Device clone() {
        return new WashingMachine(this.getName(), this.getConsumption());
    }

    private void notifyWashCompletion() {
        raiseEvent(new Event(this, this.getRoom(), "Wash finished"));
    }

    private void run(int time) {
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                notifyWashCompletion();
                turnOff();
                t.cancel();
            }
        };
        t.schedule(task, time* 1000L);
    }

    public void ecoWash() {
        if (getState() == State.ON) {
            return;
        }
        turnOn();
        run(20);
    }

    public void normalWash() {
        if (getState()==State.ON) {
            return;
        }
        turnOn();
        run(40);
    }
    public void superWash() {
        if (getState()==State.ON) {
            return;
        }
        turnOn();
        run(60);
    }
}
