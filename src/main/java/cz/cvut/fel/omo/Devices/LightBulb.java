package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Sensors.MotionSensor;
import cz.cvut.fel.omo.Devices.Utils.State;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventHandler;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class LightBulb extends AbstractSmartDevice implements EventHandler {

    private Color color;
    private boolean handleMotion;
    private Timer timer;

    public LightBulb(String name, int consumption) {
        super(name, consumption);
        handleMotion = false;
        timer = new Timer();
        color = Color.yellow;
    }
    public LightBulb(String name, int consumption, boolean handleMotion) {
        super(name, consumption);
        this.handleMotion = handleMotion;
        timer = new Timer();
        color = Color.yellow;
    }

    @Override
    public Device clone() {
        return new LightBulb(this.getName(),this.getConsumption());
    }

    public void changeColor(Color c) {
        if (c!= color) {
            raiseEvent(new Event(this, this.getRoom(), "Lightbulb changed color from " + color + " to " + c));
            color = c;
        }
    }

    @Override
    public void handleEvent(Event e) {
        if (e.getSourceLocation()==getRoom() && e.getSource() instanceof MotionSensor) {
            registerMotion();
        }
    }
    private void registerMotion() {
        if (!handleMotion) {
            return;
        }
        turnOnForWhile();
    }
    private void turnOnForWhile() {
        if (getState()== State.ON || timer.purge()!=0) {
            return;
        }
        turnOn();
        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                turnOff();
                timer.cancel();
            }
        };
        timer.schedule(task, 10000);
    }
    @Override
    public void turnOn() {
        super.turnOn();
        if (timer !=null) {
            timer.cancel();
        }
    }
    public void allowMotionHandling() {
        handleMotion = true;
    }
    public void disableMotionHandling() {
        handleMotion = false;
    }
}
