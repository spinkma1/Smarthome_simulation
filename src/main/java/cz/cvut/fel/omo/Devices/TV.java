package cz.cvut.fel.omo.Devices;

public class TV extends AbstractSmartDevice {
    private int volume;
    private int channel;

    public TV(String name, int consumption) {
        super(name, consumption);
        volume=10;
        channel=1;
    }

    @Override
    public TV clone() {
        return new TV(this.getName(), this.getConsumption());
    }

    public void volumeUp() {
        if (volume < 99) {
            volume++;
        }
    }
    public void volumeDown() {
        if (volume >0) {
            volume --;
        }
    }
    public void setChannel(int channel) {
        if (channel >0) {
            this.channel= channel;
        }
    }

    public int getVolume() {
        return volume;
    }

    public int getChannel() {
        return channel;
    }
}
