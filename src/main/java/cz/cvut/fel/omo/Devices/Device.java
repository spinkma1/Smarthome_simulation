package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.*;
import cz.cvut.fel.omo.Devices.Utils.Condition;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventMaker;
import cz.cvut.fel.omo.Events.EventType;
import cz.cvut.fel.omo.House.Room;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Represents an abstract device with common properties and behaviors.
 * Implements the EventMaker and Prototype interfaces.
 */
public abstract class Device implements EventMaker, Prototype {

    /** The condition of the device. */
    private Condition condition;

    /** The name of the device. */
    private String name;

    /** The manual or instructions for the device. */
    private String manual;

    /** The power consumption of the device in Watts per Hour. */
    private int consumption;

    /** The unique identifier of the device. */
    private final int id;

    /** Timestamp for the last recorded consumption metrics. */
    private LocalDateTime lastConsumptionMetrics;

    /** The room where the device is located. */
    private Room room;

    /**
     * Initializes a Device with a specified name and consumption.
     *
     * @param name        The name of the device.
     * @param consumption The power consumption of the device in Watts per Hour.
     */
    public Device(String name, int consumption) {
        id = UniqueID.getUniqueID();
        condition = Condition.NEW;
        this.name = name;
        this.consumption = consumption;
        lastConsumptionMetrics = LocalDateTime.now();
    }

    /** Returns the unique identifier of the device. */
    public int getId() {
        return id;
    }

    /** Returns the condition of the device. */
    public Condition getCondition() {
        return condition;
    }

    /** Returns the name of the device. */
    public String getName() {
        return name;
    }

    /** Returns the manual or instructions for the device. */
    public String getManual() {
        if (manual == null) {
            return "Manual not found";
        }
        return manual;
    }

    /** Returns the power consumption of the device. */
    public int getConsumption() {
        return consumption;
    }

    /** Sets the manual or instructions for the device. */
    public void setManual(String manual) {
        this.manual = manual;
    }

    /** Sets the condition of the device. */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    /** Sets the name of the device. */
    public void setName(String name) {
        this.name = name;
    }

    /** Sets the power consumption of the device. */
    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    /** Returns the room where the device is located. */
    public Room getRoom() {
        return room;
    }

    /** Sets the room where the device is located. */
    public void setRoom(Room room) {
        this.room = room;
    }

    /** Raises an event by distributing it through the EventBroker. */
    @Override
    public final void raiseEvent(Event e) {
        EventBroker.getInstance().distributeEvent(e);
        if(condition!=Condition.BROKEN){
            this.toRiskDestruction();
        }
    }

    /** Clones the device object. */
    public abstract Device clone();

    /** Returns a string representation of the device. */
    @Override
    public String toString() {
        return new StringBuilder(64).append(this.getClass().getSimpleName())
                .append(' ')
                .append(this.name)
                .append(" id:")
                .append(this.id).toString();
    }

    /**
     * Calculates the energy consumption in kiloWattHours.
     *
     * @return The consumed kiloWattHours since the last metrics.
     */
    public double getConsumedEnergy() {
        double hoursDeviceHasBeenTurnedON = (Duration.between(lastConsumptionMetrics, LocalDateTime.now()).toSeconds()) / 3600d / 1000d;
        lastConsumptionMetrics = LocalDateTime.now();
        return hoursDeviceHasBeenTurnedON * consumption;
    }

    /** Sets the timestamp for the last recorded consumption metrics. */
    protected void setLastConsumptionMetrics() {
        lastConsumptionMetrics = LocalDateTime.now();
    }

    /** Returns the timestamp for the last recorded consumption metrics. */
    protected LocalDateTime getLastConsumptionMetrics() {
        return lastConsumptionMetrics;
    }
    public void repairDevice(){
        if(manual!=null && condition==Condition.BROKEN){
            Timer t = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    setCondition(Condition.REPAIRED);
                    t.cancel();
                }
            };
            t.schedule(task, 10000);
        }
    }
    private void toRiskDestruction(){
        int randomNumber = new Random().nextInt(20) + 1;
        if (randomNumber == 1) {
            this.setCondition(Condition.BROKEN);
            raiseEvent(new Event(this, this.getRoom(), this.toString() +" has been broken", EventType.PROBLEM));
        }
    }
}