package cz.cvut.fel.omo.Devices.Sensors;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventType;

/**
 * Represents a SmokeSensor device, extending the {@link Device} class.
 * It detects smoke and triggers an event when smoke is detected.
 */
public class SmokeSensor extends Device {

    /**
     * Initializes a SmokeSensor object with a specified name and consumption.
     *
     * @param name The name of the smoke sensor.
     * @param consumption The power consumption of the smoke sensor.
     */
    public SmokeSensor(String name, int consumption) {
        super(name, consumption);
    }

    /**
     * Creates a copy of the SmokeSensor object.
     *
     * @return A new SmokeSensor object cloned from the current instance.
     */
    @Override
    public SmokeSensor clone() {
        return new SmokeSensor(this.getName(), this.getConsumption());
    }

    /**
     * Notifies the detection of smoke by raising an event with the smoke sensor as the source.
     * The event indicates the registration of smoke and a possible fire, categorized as a problem.
     */
    public void notifySmoke() {
        raiseEvent(new Event(this, this.getRoom(), "registered smoke, possible FIRE!!!", EventType.PROBLEM));
    }
}

