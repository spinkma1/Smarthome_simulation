package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Sensors.WeatherSensor;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventHandler;

/**
 * Represents a smart blind device that can be controlled and reacts to weather conditions.
 * Extends the AbstractSmartDevice class and implements the EventHandler interface to handle events.
 */
public class Blind extends AbstractSmartDevice implements EventHandler {

    /** Indicates if the blind reacts to weather conditions. */
    private boolean reactToWeather;

    /** Indicates whether the blind is currently opened or closed. */
    private boolean isOpened;

    /**
     * Initializes a Blind with a specified name.
     * Sets the blind as opened by default, and enables reacting to weather conditions.
     * Adds the blind to the event handlers in the EventBroker.
     *
     * @param name The name of the blind device.
     */
    public Blind(String name) {
        super(name, 0);
        isOpened = true;
        reactToWeather = true;
        EventBroker.getInstance().addToEventHandlers(this);
    }

    /**
     * Clones the Blind object.
     *
     * @return A new instance of the Blind object with the same name as the original.
     */
    @Override
    public Blind clone() {
        return new Blind(this.getName());
    }

    /**
     * Handles events, specifically weather-related events, to control the blind based on weather conditions.
     *
     * @param e The event to be handled.
     */
    @Override
    public void handleEvent(Event e) {
        if (e.getSource() instanceof WeatherSensor) {
            String message = e.getMessage();
            if (!reactToWeather) {
                return; // Do not react to weather events if disabled.
            }
            if (message.contains("rainy") || message.contains("foggy") || message.contains("storm")) {
                lowerBlinds(); // Lower the blinds in bad weather conditions.
            } else if (message.contains("sunny") || message.contains("clear")) {
                raiseBlinds(); // Raise the blinds in good weather conditions.
            }
        }
    }

    /**
     * Raises the blinds if they are not already opened.
     * Increases the consumed energy and raises an event indicating the blinds have been opened.
     */
    public void raiseBlinds() {
        beginFunction(); // Ensures the blind is turned on.
        if (isOpened) {
            return; // If already opened, no action needed.
        }
        isOpened = true; // Set the blinds as opened.
        consumedKWH += 0.01d; // Increase consumed energy by a fixed amount.
        raiseEvent(new Event(this, this.getRoom(), "Blinds have been opened"));
    }

    /**
     * Lowers the blinds if they are not already closed.
     * Increases the consumed energy and raises an event indicating the blinds have been lowered.
     */
    public void lowerBlinds() {
        beginFunction(); // Ensures the blind is turned on.
        if (!isOpened) {
            return; // If already closed, no action needed.
        }
        isOpened = false; // Set the blinds as closed.
        consumedKWH += 0.01d; // Increase consumed energy by a fixed amount.
        raiseEvent(new Event(this, this.getRoom(), "Blinds have been lowered"));
    }

    /**
     * Enables the blind to react to weather conditions.
     */
    public void reactToWeather() {
        reactToWeather = true;
    }

    /**
     * Disables the blind from reacting to weather conditions.
     */
    public void dontReactToWeather() {
        reactToWeather = false;
    }
}

