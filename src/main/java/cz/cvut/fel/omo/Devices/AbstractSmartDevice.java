package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Utils.State;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.House.Room;

import java.time.Duration;

/**
 * An abstract representation of a smart device that extends the {@link Device} class.
 * Manages the state, energy consumption, and functionality of smart devices.
 */
public abstract class AbstractSmartDevice extends Device {

    /** The total consumed kilowatt-hours (KWH) by the device. */
    protected double consumedKWH;

    /** The current state of the smart device. */
    private State state;

    /**
     * Initializes an AbstractSmartDevice with a specified name and consumption.
     * Sets the initial state to OFF upon creation.
     *
     * @param name The name of the smart device.
     * @param consumption The power consumption of the smart device.
     */
    public AbstractSmartDevice(String name, int consumption) {
        super(name, consumption);
        this.state=State.OFF;
    }
    /**
     * Turns off the smart device if it is currently ON.
     * Updates the consumed energy and raises an event indicating the device was turned OFF.
     */
    public void turnOff() {
        if (state  ==State.ON) {
            state = State.OFF;
            consumedKWH += super.getConsumedEnergy();
            Event event = new Event(this, getRoom(), " turned OFF");
            raiseEvent(event);
        }
    }
    /**
     * Turns on the smart device if it is currently OFF.
     * Sets the last consumption metrics, updates the state, and raises an event indicating the device was turned ON.
     */
    public void turnOn() {
        if (state  ==State.OFF) {
            state = State.ON;
            setLastConsumptionMetrics();
            Event event = new Event(this, getRoom(), " turned ON");
            raiseEvent(event);
        }
    }
    /**
     * Initiates the device's function by turning it on if it is currently OFF.
     */
    protected void beginFunction() {
        if (state == State.OFF) {
            turnOn();
        }
    }
    /**
     * Retrieves the current state of the smart device.
     *
     * @return The state of the smart device (ON or OFF).
     */
    public State getState() {
        return state;
    }
    /**
     * Sets the state of the smart device.
     *
     * @param s The state to be set for the smart device.
     */
    protected void setState(State s) {
        state=s;
    }
    /**
     * Retrieves the total consumed energy by the device.
     * Resets the consumed KWH and updates the value based on the device's state.
     *
     * @return The total consumed energy by the smart device.
     */
    @Override
    public double getConsumedEnergy() {
        double value;
        if (state == State.ON) {
            value = super.getConsumedEnergy() + consumedKWH;
        } else {
            value = consumedKWH;
            setLastConsumptionMetrics();
        }
        consumedKWH = 0d;
        return value;
    }
}
