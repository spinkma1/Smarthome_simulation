package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Devices.Utils.Food;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventType;
import java.util.ArrayList;
import java.util.List;

public class Fridge extends AbstractSmartDevice {
    private List<Food> food;

    public Fridge(String name, int consumption) {
        super(name, consumption);
        food = new ArrayList<>();
        this.turnOn();
    }

    @Override
    public Fridge clone() {
        return new Fridge(this.getName(), this.getConsumption());
    }
    private void checkFridge() {
        for (Food f: food) {
            if (f.isExperired()) {
                raiseEvent(new Event(this, this.getRoom(), f.getName() + " is expired!!!", EventType.PROBLEM));
            }
        }
    }

    private  Food getFood(String name) {
        for (Food f: food) {
            if (f.getName() == name) {
                food.remove(f);
                return f;
            }
        }
        return null;
    }

    public void accessFridge(List<Food> foodies) {
        checkFridge();
        food.addAll(foodies);
    }

    public void accessFridge(Food f) {
        checkFridge();
        food.add(f);
    }

    public Food accessFridge(String name) {
        checkFridge();
        return getFood(name);
    }
    public void addFood(Food food1){
        food.add(food1);
    }
    public void disposeExpiredFood() {
        for (Food f: food) {
            if (f.isExperired()) {
                food.remove(f);
            }
        }
    }
    @Override
    public void turnOff() {
        //Fridge cannot be turned off
    }
}
