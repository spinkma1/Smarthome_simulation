package cz.cvut.fel.omo.Devices.Sensors;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.Event;

/**
 * Represents a MotionSensor device, extending the {@link Device} class.
 * It detects motion and triggers an event when motion is detected.
 */
public class MotionSensor extends Device {

    /**
     * Initializes a MotionSensor object with a specified name and consumption.
     *
     * @param name The name of the motion sensor.
     * @param consumption The power consumption of the motion sensor.
     */
    public MotionSensor(String name, int consumption) {
        super(name, consumption);
    }

    /**
     * Creates a copy of the MotionSensor object.
     *
     * @return A new MotionSensor object cloned from the current instance.
     */
    @Override
    public MotionSensor clone() {
        return new MotionSensor(this.getName(), this.getConsumption());
    }

    /**
     * Notifies motion detection by raising an event with the motion sensor as the source.
     * The event is associated with the room where the motion sensor is located.
     */
    public void notifyMotion() {
        raiseEvent(new Event(this, this.getRoom(), "Motion detected"));
    }
}

