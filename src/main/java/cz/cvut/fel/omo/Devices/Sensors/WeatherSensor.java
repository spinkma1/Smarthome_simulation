package cz.cvut.fel.omo.Devices.Sensors;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.Event;

import java.util.*;
/**
 * Represents a WeatherSensor device that detects and notifies weather changes.
 * Extends the {@link Device} class.
 */
public class WeatherSensor extends Device {
    public static String[] NICE_WEATHERS = {"moderate", "sunny", "clear"};
    public static String[] UGLY_WEATHERS = {"moderate", "rainy", "storm", "foggy", "cloudy"};
    private int currentTemperature;
    private String weather;
    /**
     * Initializes a WeatherSensor object with a specified name and consumption.
     * Default temperature is set to 12°C and weather is set to "moderate".
     * Notifies the initial weather status upon creation.
     *
     * @param name The name of the weather sensor.
     * @param consumption The power consumption of the weather sensor.
     */
    public WeatherSensor(String name, int consumption) {
        super(name, consumption);
        currentTemperature = 12;
        weather = "moderate";
        notifyWeather();
    }
    /**
     * Creates a copy of the WeatherSensor object.
     *
     * @return A new WeatherSensor object cloned from the current instance.
     */
    @Override
    public WeatherSensor clone() {
        return new WeatherSensor(this.getName(), this.getConsumption());
    }
    /**
     * Notifies the detected weather status based on temperature and conditions.
     * It randomly determines the weather and temperature and raises an event to notify the room.
     * The method schedules itself to run again every 60 seconds to simulate weather updates.
     */
    public void notifyWeather() {
        if (weather== "moderate") {
            if(currentTemperature >15) {
                if (Math.random() >0.3) {
                    notifyNiceWeather();
                } else {
                    notifyBadWeather();
                }
            } else {
                if (Math.random() >0.3) {
                    notifyBadWeather();
                } else {
                    notifyNiceWeather();
                }
            }
        } else  {
            if (weather== NICE_WEATHERS[1] || weather == NICE_WEATHERS[2]) {
                notifyNiceWeather();
            } else {
                notifyBadWeather();
            }
        }
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                notifyWeather();
                t.cancel();
            }
        };
        t.schedule(task, 60000);
    }
    /**
     * Notifies about unpleasant weather conditions by updating temperature and weather status.
     * Raises an event indicating the detected weather change in the room.
     */
    private void notifyBadWeather() {
        Random r = new Random();
        int addToTemperature = r.nextInt(3)+1;
        if (r.nextBoolean()) {
            addToTemperature*=-1;
        }
        currentTemperature += addToTemperature;
        weather = UGLY_WEATHERS[r.nextInt(4)];
        String message = "Weather is " + weather + ' ' + currentTemperature + "°C";
        raiseEvent(new Event(this, this.getRoom(), message));
    }
    /**
     * Notifies about pleasant weather conditions by updating temperature and weather status.
     * Raises an event indicating the detected weather change in the room.
     */
    private void notifyNiceWeather() {
        Random r = new Random();
        int addToTemperature = r.nextInt(3)+1;
        if (r.nextBoolean()) {
            addToTemperature*=-1;
        }
        currentTemperature += addToTemperature;
        weather = NICE_WEATHERS[r.nextInt(3)];
        String message = "Weather is " + weather + ' ' + currentTemperature + "°C";
        raiseEvent(new Event(this, this.getRoom(), message));
    }
}
