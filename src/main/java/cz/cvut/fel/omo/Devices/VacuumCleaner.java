package cz.cvut.fel.omo.Devices;

import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.House.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class VacuumCleaner extends AbstractSmartDevice{
    private int batery;
    private Room base;
    private List<Room> roomsToClean;
    private List<Room> cleaned;
    private boolean charging;

    public VacuumCleaner(String name, int consumption) {
        super(name, consumption);
        this.batery = 50;
        this.base = this.getRoom();
        this.cleaned = new ArrayList<>();
        this.charging = false;
    }

    @Override
    public Device clone() {
        return new VacuumCleaner(this.getName(), this.getConsumption());
    }

    public void cleanCurrentRoom() throws Exception {
        if (charging) {
            throw new Exception("VacuumCleaner must be charged!!!");
        }
        this.beginFunction();
        if (this.getRoom().getArea()*2 < batery +5) {
            Timer t = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    notifyCleaning();
                    batery -= getRoom().getArea()*2;
                    dock();
                    t.cancel();
                }
            };
            t.schedule(task, (this.getRoom().getArea())* 1000L);
        } else {
            notifyLowBatery();
            dock();
        }
    }

    private void notifyCleaning() {
        raiseEvent(new Event(this, this.getRoom()," cleaned room "));
    }

    private void notifyLowBatery() {
        raiseEvent(new Event(this, this.getRoom(), " not enough battery to clean room"));
    }
    public void cleanWholeLevel() throws Exception {
        if (charging) {
            throw new Exception("VacuumCleaner must be charged!!!");
        }
        this.beginFunction();
        roomsToClean = base.getFloor().getRooms();
        cleaned.clear();
        cleanRoom();
    }

    private void cleanRoom() {
        if (getRoom().getArea()*2 >= batery +5) {
            notifyLowBatery();
            return;
        }
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                notifyCleaning();
                batery -= getRoom().getArea()*2;
                cleaned.add(getRoom());
                Room r = getNextRoom();
                if (r != null) {
                    setRoom(r);
                    cleanRoom();
                } else {
                    dock();
                }
                t.cancel();
            }
        };
        t.schedule(task, (this.getRoom().getArea())* 1000L);
    }
    private Room getNextRoom() {
        for (Room r : roomsToClean) {
            if (!cleaned.contains(r)) {
                return r;
            }
        }
        return null;
    }
    public void dock() {
        batery-=5;
        this.setRoom(base);
        charge();
        this.turnOff();
    }
    private void charge() {
        charging = true;
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                consumedKWH += ((100-batery) *getConsumption())/30d;
                batery = 100;
                charging = false;
                notifyCharging();
                t.cancel();
            }
        };
        int time = (100 - batery) * 500;
        t.schedule(task, time);
    }
    private void notifyCharging() {
        raiseEvent(new Event(this, this.getRoom()," is fully charged"));
    }
    @Override
    public void setRoom(Room room) {
        super.setRoom(room);
        if (base== null) {
            base = room;
        }
    }
    @Override
    public double getConsumedEnergy() {
        double value = consumedKWH;
        consumedKWH = 0;
        return value;
    }
}
