package cz.cvut.fel.omo.Creatures;

import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventMaker;
import cz.cvut.fel.omo.Events.EventType;

/**
 * Represents a Dog, extending the Creature class.
 */
public class Dog extends Creature implements EventMaker {

    /**
     * Constructor for creating a Dog object.
     *
     * @param age      The age of the dog.
     * @param gender   The gender of the dog.
     * @param name     The name of the dog.
     * @param nickname The nickname of the dog.
     */
    public Dog(int age, String gender, String name, String nickname) {
        super(age, gender, name, nickname);
    }

    /**
     * Simulates the action of the dog making a poop and raises a 'PROBLEM' event.
     */
    public void makeAPoop() {
        Event e;
        if (this.getRoom() == null) {
            e = new Event(this, getRoom(), " pooped on", EventType.PROBLEM);
        } else {
            e = new Event(this, getRoom(), " pooped in the room: ", EventType.PROBLEM);
        }
        raiseEvent(e);
    }

    /**
     * Simulates the action of the dog barking and raises an 'INFO' event.
     */
    public void bark() {
        raiseEvent(new Event(this, getRoom(), " barked.", EventType.INFO));
    }

    /**
     * Simulates the action of the dog jumping and raises an 'INFO' event.
     */
    public void jump() {
        raiseEvent(new Event(this, getRoom(), " jumped.", EventType.INFO));
    }
}
