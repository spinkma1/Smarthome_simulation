package cz.cvut.fel.omo.Creatures;

import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventType;

/**
 * Represents a child, extending the Person class.
 */
public class Child extends Person {

    /**
     * Constructor for creating a Child object.
     *
     * @param age      The age of the child.
     * @param gender   The gender of the child.
     * @param name     The name of the child.
     * @param nickname The nickname of the child.
     */
    public Child(int age, String gender, String name, String nickname) {
        super(age, gender, name, nickname);
    }

    /**
     * Simulates the action of drinking and raises an 'INFO' event.
     */
    public void drink() {
        raiseEvent(new Event(this, getRoom(), "drank a drink", EventType.INFO));
    }

    /**
     * Simulates the action of playing and raises an 'INFO' event.
     */
    public void play() {
        raiseEvent(new Event(this, getRoom(), "played with Lego", EventType.INFO));
    }

    /**
     * Simulates the action of making poop and raises a 'PROBLEM' event if the child is under 4 years old.
     */
    public void makePoop() {
        if (getAge() < 4) {
            raiseEvent(new Event(this, getRoom(), "pooped in the diaper", EventType.PROBLEM));
        }
    }

    /**
     * Simulates the action of making the bed and raises an 'INFO' event if the child is over 12 years old.
     */
    public void makeBed() {
        if (getAge() > 12) {
            raiseEvent(new Event(this, getRoom(), "made bed", EventType.INFO));
        }
    }

    /**
     * Simulates the action of crying and raises a 'PROBLEM' event.
     */
    public void cry() {
        raiseEvent(new Event(this, getRoom(), "cried", EventType.PROBLEM));
    }
}
