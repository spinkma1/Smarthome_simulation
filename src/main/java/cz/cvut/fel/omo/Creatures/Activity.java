package cz.cvut.fel.omo.Creatures;

import java.time.LocalDateTime;

/**
 * Class representing an activity.
 */
public class Activity {
    private LocalDateTime begin;
    private LocalDateTime end;
    private String name;
    private ActivityMaker activityMaker;

    /**
     * Constructor for an activity with specified start and name.
     *
     * @param activityMaker The maker of the activity.
     * @param begin         Start of the activity.
     * @param name          Name of the activity.
     */
    public Activity(ActivityMaker activityMaker, LocalDateTime begin, String name) {
        this.activityMaker = activityMaker;
        this.begin = begin;
        this.name = name;
    }

    /**
     * Constructor for an activity with specified start, end, and name.
     *
     * @param activityMaker The maker of the activity.
     * @param begin         Start of the activity.
     * @param end           End of the activity.
     * @param name          Name of the activity.
     */
    public Activity(ActivityMaker activityMaker, LocalDateTime begin, LocalDateTime end, String name) {
        this.activityMaker = activityMaker;
        this.begin = begin;
        this.end = end;
        this.name = name;
    }

    /**
     * Sets the end time of the activity.
     *
     * @param end End time of the activity.
     */
    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    /**
     * Gets the start time of the activity.
     *
     * @return Start time of the activity.
     */
    public LocalDateTime getBegin() {
        return begin;
    }

    /**
     * Gets the end time of the activity.
     *
     * @return End time of the activity.
     */
    public LocalDateTime getEnd() {
        return end;
    }

    /**
     * Gets the name of the activity.
     *
     * @return Name of the activity.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the activity maker.
     *
     * @return Activity maker.
     */
    public ActivityMaker getActivityMaker() {
        return activityMaker;
    }

    /**
     * Sets the activity maker.
     *
     * @param activityMaker Activity maker.
     */
    public void setActivityMaker(ActivityMaker activityMaker) {
        this.activityMaker = activityMaker;
    }
}
