package cz.cvut.fel.omo.Creatures;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Devices.Sensors.MotionSensor;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventMaker;
import cz.cvut.fel.omo.Events.EventType;
import cz.cvut.fel.omo.House.Floor;
import cz.cvut.fel.omo.House.Outside;
import cz.cvut.fel.omo.House.Room;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
/**
 * An abstract class representing a Creature that implements EventMaker interface.
 */
public abstract class Creature implements EventMaker {
    private int age;
    private String gender;


    private String name;

    private String nickname;
    private Room room;


    private Outside outside;
    /**
     * Constructor for creating a Creature object.
     *
     * @param age      The age of the creature.
     * @param gender   The gender of the creature.
     * @param name     The name of the creature.
     * @param nickname The nickname of the creature.
     */
    public Creature(int age, String gender, String name, String nickname) {
        this.age = age;
        this.gender = gender;
        this.name = name;
        this.nickname = nickname;
    }
    /**
     * Enters the house if outside and not already in a room.
     * Moves the creature to a room in the house and triggers an 'INFO' event.
     */
    public void enterHouse(){
        if(room==null&&outside!=null){
            if(!this.getOutside().getSmartHome().getHouse().getFloors().isEmpty()){
                List<Floor> floors =this.getOutside().getSmartHome().getHouse().getFloors();
                Collections.sort(floors, Comparator.comparingInt(Floor::getLevel));
                boolean found=false;

                for (Floor floor:floors) {
                    if(found){
                        break;
                    }
                    for(Room room: floor.getRooms()){
                        room.addCreature(this);
                        outside.removeCreature(this);
                        raiseEvent(new Event(this,getRoom(),this.getName() + " entered the house. (Room: "+getRoom().getName()+")",EventType.INFO));
                        found=true;
                        checkMotionSensor();
                        break;

                    }
                }
            }
        }
    }
    /**
     * Leaves the house if inside and in a room.
     * Moves the creature outside the house and triggers an 'INFO' event.
     */
    public void leaveHouse(){
        if(room!=null&&outside==null){
            if(!this.getRoom().getFloor().getHouse().getFloors().isEmpty()){
                List<Floor> floors =this.getRoom().getFloor().getHouse().getFloors();
                boolean found=false;

                for (Floor floor:floors) {
                    if(found){
                        break;
                    }
                    for(Room room: floor.getRooms()){
                        if(room==this.room){
                            this.getRoom().getFloor().getHouse().getSmartHome().getOutside().addCreature(this);
                            raiseEvent(new Event(this,getRoom(),this.getName() + " left the house.", EventType.INFO));
                            room.removeCreature(this);
                            found=true;
                            break;
                        }
                    }
                }
            }
        }

    }
    /**
     * Raises an event using the EventBroker.
     *
     * @param e The event to be raised.
     */
    @Override
    public void raiseEvent(Event e) {
        EventBroker.getInstance().distributeEvent(e);
    }
    /**
     * Moves the creature to a different room and triggers an 'INFO' event.
     *
     * @param room The room to move the creature into.
     */
    public void move(Room room){
        if(this.room!=room&&room!=null){
            this.getRoom().getCreatures().remove(this);
            room.addCreature(this);
            raiseEvent(new Event(this,getRoom(),this.getName() + " entered room: "+room.getName()+".",EventType.INFO));
            checkMotionSensor();
        }
    }
    /**
     * Sets the nickname of the creature.
     *
     * @param nickname The nickname to set.
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    /**
     * Checks for a motion sensor in the room and notifies if found.
     * If a creature enters a room containing a motion sensor, the sensor notifies by light in that room.
     */
    private void checkMotionSensor(){
        List<Device> allDevices = this.getRoom().getFloor().getHouse().getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room1 -> room1.getDevices().stream())
                .collect(Collectors.toList());
            /*
                When creature enters room, there might be motion sensor.
                Motion sensor will notify everyone by light in that particular room
             */
        for (Device device:allDevices) {
            if(device instanceof MotionSensor && device.getRoom()==this.getRoom()){
                ((MotionSensor) device).notifyMotion();;
            }
        }
    }
    /**
     * Retrieves the room where the creature is located.
     *
     * @return The current room of the creature.
     */
    public Room getRoom(){
        return room;
    }
    /**
     * Sets the room where the creature should be located.
     *
     * @param room The room to set for the creature.
     */
    public void setRoom(Room room){
        this.room=room;
    }
    /**
     * Retrieves the name of the creature.
     *
     * @return The name of the creature.
     */
    public String getName() {
        return name;
    }
    /**
     * Retrieves the outside area where the creature is located.
     *
     * @return The outside area where the creature is situated.
     */
    public Outside getOutside() {
        return outside;
    }
    /**
     * Sets the outside area where the creature should be located.
     *
     * @param outside The outside area to set for the creature.
     */

    public void setOutside(Outside outside) {
        this.outside = outside;
    }

    /**
     * Retrieves the nickname of the creature.
     *
     * @return The nickname of the creature.
     */
    public String getNickname() {
        return nickname;
    }
    /**
     * Retrieves the age of the creature.
     *
     * @return The age of the creature.
     */
    public int getAge() {
        return age;
    }
    /**
     * Sets the age of the creature.
     *
     * @param age The age to set for the creature.
     */
    public void setAge(int age) {
        this.age = age;
    }
    /**
     * Represents the Creature object as a string.
     *
     * @return A string representation of the Creature object.
     */
    @Override
    public String toString() {
        return new StringBuilder(64).append(this.getClass().getSimpleName())
                .append(' ')
                .append(this.name)
                .append(" age:")
                .append(this.age)
                .append(" gender:")
                .append(this.gender).toString();

    }

}
