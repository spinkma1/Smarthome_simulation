package cz.cvut.fel.omo.Creatures;


import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Devices.Fridge;
import cz.cvut.fel.omo.Devices.Heater;
import cz.cvut.fel.omo.Devices.TV;
import cz.cvut.fel.omo.Devices.Utils.Food;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventHandler;
import cz.cvut.fel.omo.Events.EventType;
import cz.cvut.fel.omo.House.Floor;
import cz.cvut.fel.omo.House.Room;


import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
/**
 * Represents a Father, extending the {@link Person} class and implementing {@link EventHandler}.
 * This class models a father within the household.
 */
public class Father extends Person implements EventHandler {
    private Queue<Event> unhandledEvents;
    /**
     * Constructor for creating a Father object.
     *
     * @param age      The age of the father.
     * @param gender   The gender of the father.
     * @param name     The name of the father.
     * @param nickname The nickname of the father.
     */
    public Father(int age, String gender, String name, String nickname) {
        super(age, gender, name, nickname);
        unhandledEvents=new LinkedList<>();
    }
    /**
     * Handles unhandled events in the queue.
     * Processes the unhandled events, fixes issues if possible, and sends corresponding events to the EventBroker.
     */
    public void handleUnhandledEvent(){
        if(!unhandledEvents.isEmpty()){
            for(Event e :unhandledEvents){
                if(fixEvent(e)){
                    EventBroker.getInstance().distributeEvent(new Event(this, this.getRoom(), String.valueOf(e.getId()), EventType.FIXED));
                }
                //na event brokera by se při řešení eventů posílaly nové eventy - takové, že se něco vyřešilo
                //a event broker si tu událost odmaže z nevyřešených a všem pošle řešící událost - ti si ji pak smažou taky
            }
        }
    }
    /**
     * Handles the incoming event.
     * If the event type is 'INFO', it's ignored. For 'FIXED' events, removes resolved events from the unhandled queue.
     * Adds unhandled events to the queue if they're not already present and not 'FIXED'.
     *
     * @param e The event to be handled.
     */
    @Override
    public void handleEvent(Event e) {
        if(e.getType()==EventType.INFO){
            return;
        }
        if (e.getType() == EventType.FIXED) {
            int id = Integer.parseInt(e.getMessage());
            for (Event event : unhandledEvents) {
                if (event.getId() == id) {
                    unhandledEvents.remove(event);
                }
            }
            //takhle by se osobám z fronty odebíraly již vyřešené události
        }
        if (!unhandledEvents.contains(e)&&e.getType() != EventType.FIXED) {
            unhandledEvents.add(e);
        }
    }
    /**
     * Fixes specific events by taking action based on the event message.
     *
     * @param e The event to be fixed.
     * @return True if the event was successfully fixed; otherwise, false.
     */
    private boolean fixEvent(Event e){
        String founded=importantEvent(e.getMessage());
        if(founded!=null){
            switch (founded) {
                case " has been broken":
                    if(getRoom()==e.getSourceLocation()){
                        repairDevice(e.getId());
                        return true;
                    }else{
                        move(e.getSourceLocation());
                        repairDevice(e.getId());
                        return true;
                    }
                case "pooped in the room: ":
                    if(getRoom()==e.getSourceLocation()){
                        cleanPoop();
                        return true;
                    }else{
                        move(e.getSourceLocation());
                        cleanPoop();
                        return true;
                    }
                case "registered smoke, possible FIRE!!!":
                    if(getRoom()!=null) {
                        this.leaveHouse();
                    }
                    List<Floor> floors = this.getOutside().getSmartHome().getHouse().getFloors();
                    Collections.sort(floors, Comparator.comparingInt(Floor::getLevel));
                    Creature mom=null;
                    Creature dog=null;
                    Creature kid=null;
                    for (Floor floor : floors) {
                        for (Room room : floor.getRooms()) {
                            for(Creature creature : room.getCreatures()){
                                if(creature instanceof Mother){
                                    mom=creature;
                                }
                                if(creature instanceof Child){
                                    kid=creature;
                                }
                                if(creature instanceof Dog){
                                    dog=creature;
                                }
                            }
                        }
                    }
                    if(mom==null&&dog==null&&kid==null){
                        call911();
                        return true;
                    }
                default:
                    break;
            }
        }
        return false;
    }
    /**
     * Triggers an 'INFO' event indicating that the father called 911 and the fire is fixed, they can enter the house again.
     */
    public void call911(){
        raiseEvent(new Event(this,getRoom(),"called 911", EventType.INFO));
    }
    /**
     * Initiates or ends the exercise activity for the father.
     * If the current activity is 'exercising', ends the activity; otherwise, begins a new 'exercising' activity.
     */
    public void exercise(){
        if(getCurrentActivity()==null){
            Activity activity= new Activity(this,LocalDateTime.now(),"exercising");
            beginActivity(activity);
            setCurrentActivity(activity);
        }else if(getCurrentActivity().getName().equals("exercising")){
            Activity activity= new Activity(this,null,LocalDateTime.now(),"exercising");
            endActivity(activity);
            setCurrentActivity(null);
        }
    }
    /**
     * Turns on the TV with the specified ID if found.
     *
     * @param id The ID of the TV to turn on.
     */
    public void turnOnTV(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.turnOn();
        }
    }
    /**
     * Turns off the TV with the specified ID if found.
     *
     * @param id The ID of the TV to turn off.
     */
    public void turnOffTV(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.turnOff();
        }
    }
    /**
     * Changes the channel of the TV with the specified ID if found.
     *
     * @param id      The ID of the TV to change the channel.
     * @param channel The channel number to set.
     */
    public void changeChannelOfTV(int id, int channel){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.setChannel(channel);
        }
    }
    /**
     * Increases the volume of the TV with the specified ID if found.
     *
     * @param id The ID of the TV to increase the volume.
     */
    public void volumeUpTV(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.volumeUp();
        }
    }
    /**
     * Decreases the volume of the TV with the specified ID if found.
     *
     * @param id The ID of the TV to decrease the volume.
     */
    public void volumeDownTV(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.volumeDown();
        }
    }
    /**
     * Allows the father to drink beer from the specified fridge if available.
     *
     * @param id The ID of the fridge containing beer.
     */
    public void drinkBeer(int id){
        Device fridge=findDevice(id);
        if(fridge!=null){
            Fridge fr= (Fridge) fridge;
            if(fr.accessFridge("Beer")!=null){
                fr.accessFridge("Beer");
            }

        }
    }
    /**
     * Adds food to the specified fridge if found.
     *
     * @param id    The ID of the fridge to add food to.
     * @param food  The food item to add to the fridge.
     */
    public void addToFridge(int id, Food food){
        Device fridge=findDevice(id);
        if(fridge!=null){
            Fridge fr= (Fridge) fridge;
            fr.addFood(food);
        }
    }
    /**
     * Sets the temperature to heat in the specified heater if found.
     *
     * @param id          The ID of the heater to set the temperature for.
     * @param temperature The temperature to set in the heater.
     */
    public void setTemperatureToHeat(int id,int temperature){
        Device heater=findDevice(id);
        if(heater!=null){
            Heater heat= (Heater) heater;
            heat.setTemperatureToHeatAt(temperature);
        }
    }
    /**
     * Triggers an 'INFO' event indicating that the father drank a drink.
     */
    public void drink(){
        raiseEvent(new Event(this,getRoom(),"drank a drink", EventType.INFO));
        }
    /**
     * Triggers an 'INFO' event indicating that the father cleaned the poop in the room.
     */
    public void cleanPoop(){
        raiseEvent(new Event(this,getRoom(),"cleaned the poop in room:", EventType.INFO));
    }
    /**
     * Identifies important events from the event message.
     *
     * @param eventMsg The message from the event.
     * @return The identified important event, if found; otherwise, null.
     */
    private String importantEvent(String eventMsg){
        String founded=null;
        ArrayList<String> eventMsgs = new ArrayList<>(Arrays.asList("pooped in the room: ","registered smoke, possible FIRE!!!"," has been broken"));
        for(int i=0;i<eventMsgs.size();i++){
            if(Pattern.compile(eventMsgs.get(i)).matcher(eventMsg).find()){
                founded=eventMsgs.get(i);
            }
        }
        return founded;
    }


    /**
     * Retrieves the queue of unhandled events.
     *
     * @return The queue containing unhandled events.
     */
    public Queue<Event> getUnhandledEvents() {
        return unhandledEvents;
    }
    /**
     * Sets the queue of unhandled events.
     *
     * @param unhandledEvents The queue containing unhandled events to set.
     */
    public void setUnhandledEvents(Queue<Event> unhandledEvents) {
        this.unhandledEvents = unhandledEvents;
    }
    private void repairDevice(Integer deviceID){
        Device device=findDevice(deviceID);
        if(device!=null){
            device.repairDevice();
        }
    }
}
