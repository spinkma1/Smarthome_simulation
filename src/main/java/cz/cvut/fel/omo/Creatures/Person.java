package cz.cvut.fel.omo.Creatures;

import cz.cvut.fel.omo.Devices.Device;
import cz.cvut.fel.omo.Events.EventBroker;

import java.util.List;

/**
 * Represents an abstract Person, extending the {@link Creature} class and implementing {@link ActivityMaker}.
 * This class serves as a base for different types of individuals within the system.
 */
public abstract class Person extends Creature implements ActivityMaker {

    /**
     * Current activity the person is engaged in.
     */
    private Activity currentActivity;

    /**
     * Constructs a Person object with specified age, gender, name, and nickname.
     *
     * @param age The age of the person.
     * @param gender The gender of the person.
     * @param name The name of the person.
     * @param nickname The nickname of the person.
     */
    public Person(int age, String gender, String name, String nickname) {
        super(age, gender, name, nickname);
    }

    /**
     * Begins an activity for the person.
     * If the activity's end time is not specified, it is caught by the EventBroker.
     *
     * @param activity The activity to begin.
     */
    @Override
    public void beginActivity(Activity activity) {
        if (activity.getEnd() == null) {
            EventBroker.getInstance().catchActivity(activity);
        }
    }

    /**
     * Ends the current activity for the person.
     * The activity is caught by the EventBroker.
     *
     * @param activity The activity to end.
     */
    @Override
    public void endActivity(Activity activity) {
        EventBroker.getInstance().catchActivity(activity);
    }

    /**
     * Retrieves the current activity of the person.
     *
     * @return The current activity of the person.
     */
    public Activity getCurrentActivity() {
        return currentActivity;
    }

    /**
     * Sets the current activity of the person.
     *
     * @param currentActivity The activity to set as the current activity for the person.
     */
    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    /**
     * Finds a device by its ID within the house.
     *
     * @param id The ID of the device to find.
     * @return The device found, or null if not found.
     */
    protected Device findDevice(int id){
        return this.getRoom().getFloor().getHouse().getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getDevices().stream())
                .filter(device -> device.getId()==id).findFirst().orElse(null);
    }
}

