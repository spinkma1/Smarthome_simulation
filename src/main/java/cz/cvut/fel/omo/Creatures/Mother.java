package cz.cvut.fel.omo.Creatures;

import cz.cvut.fel.omo.Devices.*;
import cz.cvut.fel.omo.Events.Event;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.Events.EventHandler;
import cz.cvut.fel.omo.Events.EventType;
import cz.cvut.fel.omo.House.Floor;
import cz.cvut.fel.omo.House.Room;

import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
/**
 * Represents a Mother, extending the {@link Person} class and implementing {@link EventHandler}.
 * This class models a mother within the household.
 */
public class Mother extends Person implements EventHandler {

    private Queue<Event> unhandledEvents;
    /**
     * Constructor for Mother class.
     *
     * @param age      The age of the mother.
     * @param gender   The gender of the mother.
     * @param name     The name of the mother.
     * @param nickname The nickname of the mother.
     */
    public Mother(int age, String gender, String name, String nickname) {
        super(age, gender, name, nickname);
        unhandledEvents=new LinkedList<>();
    }
    /**
     * Handles unhandled events.
     * If there are unhandled events, attempts to fix them and removes the event from the unhandled events queue if solved.
     */
    public void handleUnhandledEvent(){
        if(!unhandledEvents.isEmpty()){
            for(Event e :unhandledEvents){
                if(fixEvent(e)){
                    EventBroker.getInstance().distributeEvent(new Event(this, this.getRoom(), String.valueOf(e.getId()), EventType.FIXED));
                }
                //na event brokera by se při řešení eventů posílaly nové eventy - takové, že se něco vyřešilo
                //a event broker si tu událost odmaže z nevyřešených a všem pošle řešící událost - ti si ji pak smažou taky
            }
        }
    }
    /**
     * Handles the event received.
     *
     * @param e The event to handle.
     */
    @Override
    public void handleEvent(Event e) {
        if(e.getType()==EventType.INFO){
            return;
        }
        if (e.getType() == EventType.FIXED) {
            int id = Integer.parseInt(e.getMessage());
            for (Event event: unhandledEvents) {
                if (event.getId() == id) {
                    unhandledEvents.remove(event);
                }
            }
            //takhle by se osobám z fronty odebíraly již vyřešené události
        }
        if (!unhandledEvents.contains(e)&&e.getType() != EventType.FIXED) {
            unhandledEvents.add(e);
        }
    }
    /**
     * Fixes the given event if it matches important events.
     *
     * @param e The event to fix.
     * @return True if the event was fixed; otherwise, false.
     */
    private boolean fixEvent(Event e){
        String founded=importantEvent(e.getMessage());
        if(founded!=null){
            switch (founded) {
                case "pooped in the room: ":
                    if(getRoom()==e.getSourceLocation()){
                        cleanPoop();
                        return true;
                        //checkConsintency(e);
                    }else{
                        move(e.getSourceLocation());
                        cleanPoop();
                        return true;
                        //checkConsintency(e);

                    }
                case "pooped in the diaper":
                    Child kid=(Child)e.getSource();
                    if(getRoom()==kid.getRoom()){
                        cleanBaby();
                        return true;
                    }else{
                        move(e.getSourceLocation());
                        cleanBaby();
                        return true;
                    }
                case "registered smoke, possible FIRE!!!":
                    if(this.getOutside()!=null) {
                        this.enterHouse();
                    }
                        List<Floor> floors = this.getRoom().getFloor().getHouse().getFloors();
                        Collections.sort(floors, Comparator.comparingInt(Floor::getLevel));

                        for (Floor floor : floors) {
                            for (Room room : floor.getRooms()) {
                                Creature child=null;
                                Creature dog=null;
                                for(Creature creature : room.getCreatures()){
                                    if(creature instanceof Child){
                                        child=creature;
                                    }
                                    if(creature instanceof Dog){
                                        dog=creature;
                                    }
                                }
                                if(child!=null){
                                    child.leaveHouse();
                                }
                                if(dog!=null){
                                    dog.leaveHouse();
                                }
                            }
                        }
                this.leaveHouse();
                default:
                    break;
            }
        }
        return false;
    }
    /**
     * Initiates or ends baking in the oven if no current baking activity exists.
     *
     * @param id       The ID of the oven to bake in.
     * @param degrees  The degrees for baking.
     * @param minutes  The duration for baking in minutes.
     */
    public void bake(int id,int degrees, int minutes){
        if(getCurrentActivity()==null){
            Activity activity= new Activity(this,LocalDateTime.now(),"baking");
            beginActivity(activity);
            setCurrentActivity(activity);

            Device oven=findDevice(id);
            if(oven!=null){
                Oven ov= (Oven) oven;
                ov.bake(degrees,minutes);
            }

        }
    }
    /**
     * Ends the current baking activity if the current activity is 'baking'.
     */
    public void bake(){
        if(getCurrentActivity().getName().equals("baking")){
            Activity activity= new Activity(this,null,LocalDateTime.now(),"baking");
            endActivity(activity);
            setCurrentActivity(null);
        }
    }
    /**
     * Turns on the TV corresponding to the given ID.
     *
     * @param id The identifier of the TV to be turned on.
     */
    public void turnOnTv(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.turnOn();
        }
    }
    /**
     * Turns off the TV corresponding to the given ID.
     *
     * @param id The identifier of the TV to be turned off.
     */
    public void turnOffTv(int id){
        Device tv=findDevice(id);
        if(tv!=null){
            TV television= (TV) tv;
            television.turnOff();
        }
    }
    public void vacuumCurrentRoom(int id) throws Exception {
        Device cleaner=findDevice(id);
        if(cleaner!=null){
            VacuumCleaner vacuumCleaner= (VacuumCleaner) cleaner;
            try{
                vacuumCleaner.cleanCurrentRoom();
            }catch (Exception e){
                throw new Exception("VacuumCleaner must be charged!!!");
            }
        }
    }
    public void vacuumWholeLevel(int id) throws Exception {
        Device cleaner=findDevice(id);
        if(cleaner!=null){
            VacuumCleaner vacuumCleaner= (VacuumCleaner) cleaner;
            try{
                vacuumCleaner.cleanWholeLevel();
            }catch (Exception e){
                throw new Exception("VacuumCleaner must be charged!!!");
            }
        }
    }
    public void washEco(int id){
        Device washingMachine=findDevice(id);
        if(washingMachine!=null){
            WashingMachine machine= (WashingMachine) washingMachine;
            machine.ecoWash();
        }
    }
    public void washNormal(int id){
        Device washingMachine=findDevice(id);
        if(washingMachine!=null){
            WashingMachine machine= (WashingMachine) washingMachine;
            machine.normalWash();
        }
    }
    public void washSuper(int id){
        Device washingMachine=findDevice(id);
        if(washingMachine!=null){
            WashingMachine machine= (WashingMachine) washingMachine;
            machine.superWash();
        }
    }






    public void cleanPoop(){
        raiseEvent(new Event(this,getRoom(),"cleaned the poop in the room:", EventType.INFO));
    }
    public void cleanBaby(){
        raiseEvent(new Event(this,getRoom(),"cleaned the baby", EventType.INFO));
    }
    public void calmBaby(){
        raiseEvent(new Event(this,getRoom(),"calmed the baby", EventType.INFO));
    }
    private String importantEvent(String eventMsg){
        String founded=null;
        ArrayList<String> eventMsgs = new ArrayList<>(Arrays.asList("pooped in the room: ","pooped in the diaper","cried","registered smoke, possible FIRE!!!"));
        for(int i=0;i<eventMsgs.size();i++){
            if(Pattern.compile(eventMsgs.get(i)).matcher(eventMsg).find()){
                founded=eventMsgs.get(i);
            }
        }
        return founded;
    }
    /**
     * Gets the unhandled events.
     *
     * @return The queue of unhandled events.
     */
    public Queue<Event> getUnhandledEvents() {
        return unhandledEvents;
    }
}
