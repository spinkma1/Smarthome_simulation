package cz.cvut.fel.omo.Creatures;

/**
 * Interface representing an activity maker.
 */
public interface ActivityMaker {
    /**
     * Signals the beginning of an activity.
     *
     * @param activity The activity to begin.
     */
    void beginActivity(Activity activity);

    /**
     * Signals the end of an activity.
     *
     * @param activity The activity to end.
     */
    void endActivity(Activity activity);
}
