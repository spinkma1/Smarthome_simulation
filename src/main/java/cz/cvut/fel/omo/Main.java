package cz.cvut.fel.omo;

import cz.cvut.fel.omo.Creatures.Child;
import cz.cvut.fel.omo.Creatures.Dog;
import cz.cvut.fel.omo.Creatures.Father;
import cz.cvut.fel.omo.Creatures.Mother;
import cz.cvut.fel.omo.Devices.*;
import cz.cvut.fel.omo.Devices.Sensors.SmokeSensor;
import cz.cvut.fel.omo.Devices.Sensors.WeatherSensor;
import cz.cvut.fel.omo.Devices.Utils.Condition;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.HouseBuilder;
import cz.cvut.fel.omo.House.SmartHome;
import cz.cvut.fel.omo.Reports.Loggers.FileStrategy;

import java.time.Duration;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) throws Exception {
        TV tv = new TV("LG",5000);
        Dog pes = new Dog(5,"Helicopter","Bruce","Vayne");
        Father dad = new Father(33,"Male","Pepa","Pepíček");
        Mother mom = new Mother(69,"Female","Anna","Anička");
        Mother mom2 = new Mother(679,"Female","Anna","Anička");
        Child kid = new Child(1,"Male","Josef","Pepa");
        SmartHome home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().addFloor().addRoom(1, "mučírna").addRoom(1,"garáž").build());
        home.getHouse().installDevice(tv,"mučírna");
        home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(pes);
        home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(kid);
        home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom);
        home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom2);
        home.getHouse().setAddress("K Myslivně 167");
        EventBroker.getInstance().addToEventHandlers(dad);
        EventBroker.getInstance().addToEventHandlers(mom);
        pes.makeAPoop();
        dad.enterHouse();
        EventBroker.getInstance();
        dad.handleUnhandledEvent();
        EventBroker.getInstance();
        mom.handleUnhandledEvent();
        EventBroker.getInstance();
/*
        dad.handleUnhandledEvent();
        mom.enterHouse();
        mom.handleUnhandledEvent();
        dad.handleUnhandledEvent();
        kid.makePoop();


        WashingMachine washingMachine = new WashingMachine("LG", 500);
        Fridge fridge = new Fridge("Xiomi",6000);
        VacuumCleaner cleaner = new VacuumCleaner("Xiaomi", 1000);
        home.getHouse().installDevice(washingMachine,"mučírna");
        home.getHouse().installDevice(cleaner,"mučírna");
        home.getHouse().installDevice(fridge,"mučírna");
        cleaner.turnOn();
        fridge.turnOn();
        washingMachine.turnOn();
        Thread.sleep(5000);
        Blind b = new Blind("Blinds");
        home.getHouse().installDevice(b,"mučírna");
        for (int i = 0; i < 100; i++) {
            b.lowerBlinds();
        }*/

        dad.turnOnTV(1);
        dad.turnOffTV(1);
        //System.out.println(b.getConsumedEnergy() + " kwh");
        //home.getConsumptionReport();
        dad.turnOnTV(1);
        dad.turnOffTV(1);
        mom.handleUnhandledEvent();
        pes.leaveHouse();


        for(int i=0;i<100;i++){ //zníčíme televizi xd
            tv.turnOn();
            tv.turnOff();
        }
        mom.handleUnhandledEvent();
        dad.handleUnhandledEvent();

    }
}
