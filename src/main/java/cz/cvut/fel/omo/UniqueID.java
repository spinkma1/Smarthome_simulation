package cz.cvut.fel.omo;

/**
 * UniqueID class generates unique identification numbers.
 * It provides a static method to retrieve a unique ID.
 */
public class UniqueID {
    private static int id = 0;

    /**
     * Retrieves a unique identification number.
     *
     * @return A unique identification number.
     */
    public static int getUniqueID() {
        id++;
        return id;
    }
}
