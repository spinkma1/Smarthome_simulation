package cz.cvut.fel.omo.Events;

import cz.cvut.fel.omo.Creatures.Activity;

import java.util.*;

import cz.cvut.fel.omo.Reports.Loggers.LogStrategy;
import cz.cvut.fel.omo.Reports.Loggers.SmartHomeLogger;

import javax.accessibility.AccessibleIcon;
/**
 * The EventBroker class manages events and activities within the smart home system.
 */
public class EventBroker {
    private static final SmartHomeLogger logger = new SmartHomeLogger();
    private final Set<Event> unhandledEvents;

    private final List<Event> historyOfEvents;
    private final List<Activity> currentActivities;
    private final List<Activity> historyOfActivities;
    private final List<EventHandler> eventHandlers;

    private static EventBroker instance;
    /**
     * Constructs an EventBroker instance.
     * Initializes the necessary data structures for managing events and activities.
     */
    private EventBroker () {
        this.eventHandlers = new ArrayList<EventHandler>();
        this.historyOfActivities = new ArrayList<Activity>();
        this.currentActivities = new ArrayList<Activity>();
        this.unhandledEvents = new HashSet<Event>();
        this.historyOfEvents = new ArrayList<Event>();
        instance = this;
    }
    /**
     * Retrieves the singleton instance of EventBroker.
     *
     * @return The instance of EventBroker.
     */
    public static EventBroker getInstance() {
        if (instance == null) {
            return new EventBroker();
        } else {
            return instance;
        }
    }

    public List<Event> getHistoryOfEvents() {
        return historyOfEvents;
    }

    public void resetHistoryOfEvents() {
        historyOfEvents.clear();
    }

    public List<Activity> getHistoryOfActivities() {
        return historyOfActivities;
    }

    public void resetHistoryOfActivities() {
        historyOfActivities.clear();
    }
    /**
     * Distributes the provided event to registered event handlers and manages its processing.
     * Logs the event and stores it in the history of events.
     *
     * @param event The event to be distributed.
     */

    public void distributeEvent(Event event) {
        if (event.getType() == EventType.FIXED) {
            fixEvent(event);
            return;
        }
        logger.log(event.toString());
        if (event.getType() == EventType.PROBLEM) {
            unhandledEvents.add(event);
        }
        for (EventHandler handler: eventHandlers) {
            handler.handleEvent(event);
        }
        historyOfEvents.add(event);
    }
    /**
     * Handles the resolution of a fixed event and updates the system accordingly.
     *
     * @param fixedEvent The fixed event to be resolved.
     */
    private void fixEvent(Event fixedEvent) {
        int idOfFixedEvent = Integer.parseInt(fixedEvent.getMessage());
        for (Event e: unhandledEvents) {
            if (e.getId()== idOfFixedEvent) {
                logger.log("[ " +  e.toString() + " ] was handled by " + fixedEvent.getSource().toString() + " at " + fixedEvent.getDate().toString());
                unhandledEvents.remove(e);
                for (EventHandler h: eventHandlers) {
                   h.handleEvent(fixedEvent);
                }
                return;
            }
        }
    }
    /**
     * Captures an ongoing activity and logs its start or completion based on the provided activity.
     *
     * @param activity The activity to be tracked.
     */
    public void catchActivity(Activity activity){
        if(activity.getEnd()==null){
            currentActivities.add(activity);
            logger.log(activity.getActivityMaker()+" started "+activity.getName());
        }else{
            Activity toDelete=null;
            for (Activity act:currentActivities) {
                if(act.getActivityMaker()==activity.getActivityMaker()){
                    historyOfActivities.add(new Activity(act.getActivityMaker(),act.getBegin(),activity.getEnd(),activity.getName()));
                    toDelete=act;
                }
            }
            currentActivities.remove(toDelete);
            logger.log(activity.getActivityMaker()+" stopped "+activity.getName());
        }
    }

    public void addToEventHandlers(EventHandler eventHandler){
        eventHandlers.add(eventHandler);
    }
    public void changeLogging(LogStrategy log){
        logger.setLogStrategy(log);
    }
    public void addToUnhandledEvent(Event event){
        unhandledEvents.add(event);
    }
    /**
     * Retrieves the set of unhandled events.
     *
     * @return The set containing unhandled events.
     */
    public Set<Event> getUnhandledEvents() {
        return unhandledEvents;
    }
    /**
     * Retrieves the list of current ongoing activities.
     *
     * @return The list of ongoing activities.
     */
    public List<Activity> getCurrentActivities() {
        return currentActivities;
    }
}
