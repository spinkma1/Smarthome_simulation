package cz.cvut.fel.omo.Events;


/**
 * The EventMaker interface represents an entity capable of creating and raising events.
 */
public interface EventMaker {
    /**
     * Raises an event.
     *
     * @param e The event to be raised.
     */
    void raiseEvent(Event e);
}

