package cz.cvut.fel.omo.Events;

import cz.cvut.fel.omo.House.Room;
import cz.cvut.fel.omo.UniqueID;

import java.time.LocalDateTime;
import java.util.Optional;
/**
 * Represents an event in the system.
 */
public class Event {
    private int id;
    private String message;
    private EventType type;
    private LocalDateTime date;
    private EventMaker source;
    private Room sourceLocation;
    /**
     * Constructs an event with specified parameters.
     *
     * @param source         The source of the event.
     * @param sourceLocation The location where the event occurred.
     * @param message        The message describing the event.
     * @param type           The type of the event.
     */
    public Event(EventMaker source,Room sourceLocation, String message, EventType type) {
        id = UniqueID.getUniqueID();
        date = LocalDateTime.now();
        this.source = source;
        this.message = message;
        this.type = type;
        this.sourceLocation=sourceLocation;
    }
    /**
     * Constructs an event with default type INFO.
     *
     * @param source         The source of the event.
     * @param sourceLocation The location where the event occurred.
     * @param message        The message describing the event.
     */
    public Event(EventMaker source,Room sourceLocation, String message) {
        id = UniqueID.getUniqueID();
        date = LocalDateTime.now();
        this.source = source;
        this.message = message;
        this.type = EventType.INFO;
        this.sourceLocation=sourceLocation;
    }
    /**
     * Returns the type of the event.
     *
     * @return The event type.
     */
    public EventType getType() {
        return type;
    }
    /**
     * Returns the message associated with the event.
     *
     * @return The event message.
     */
    public String getMessage() {
        return message;
    }
    /**
     * Returns the source location of the event.
     *
     * @return The source location.
     */
    public Room getSourceLocation() {
        return sourceLocation;
    }
    /**
     * Returns the source of the event.
     *
     * @return The event source.
     */
    public EventMaker getSource() {
        return source;
    }
    /**
     * Returns the date and time when the event occurred.
     *
     * @return The event date and time.
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Returns the unique identifier of the event.
     *
     * @return The event identifier.
     */
    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(type.toString())
                .append(' ')
                .append(date.toString())
                .append(' ')
                .append(source.toString())
                .append(' ')
                .append(message)
                .append(' ')
                .append(Optional.ofNullable(sourceLocation).map(Room::getName).orElse("outside"))
                .toString();
    }
}
