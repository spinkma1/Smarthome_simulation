package cz.cvut.fel.omo.Events;


/**
 * The EventHandler interface represents an entity capable of handling events.
 */
public interface EventHandler {
    /**
     * Handles the incoming event.
     *
     * @param e The event to be handled.
     */
    void handleEvent(Event e);
}
