package cz.cvut.fel.omo.Events;

/**
 * Enumeration representing different types of events.
 */
public enum EventType {
    INFO,       // Informational events
    PROBLEM,    // Events indicating a problem or issue
    FIXED       // Events indicating a problem has been fixed
}
