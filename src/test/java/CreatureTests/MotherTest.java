package CreatureTests;

import cz.cvut.fel.omo.Creatures.Child;
import cz.cvut.fel.omo.Creatures.Dog;
import cz.cvut.fel.omo.Creatures.Father;
import cz.cvut.fel.omo.Creatures.Mother;
import cz.cvut.fel.omo.Devices.Oven;
import cz.cvut.fel.omo.Devices.TV;
import cz.cvut.fel.omo.Devices.Utils.State;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.HouseBuilder;
import cz.cvut.fel.omo.House.SmartHome;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class MotherTest {
    private SmartHome home;
    private Dog pes;
    private Mother mom;
    @Test
    public void cleanPoopTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.pes = new Dog(5, "Male", "Bruce", "Vayne");
        this.mom = new Mother(33, "Female", "Tereza", "Terka");
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(pes);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom);
        EventBroker.getInstance().addToEventHandlers(mom);
        pes.makeAPoop();
        mom.handleUnhandledEvent();
        assertEquals(pes.getRoom().getName(), mom.getRoom().getName());
        assertEquals(mom.getUnhandledEvents().isEmpty(),true);
    }
    @Test
    public void turningOnOffTvTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        TV televison= new TV("Xiomi",99);
        this.mom = new Mother(33, "Female", "Tereza", "Terka");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(televison);
        EventBroker.getInstance().addToEventHandlers(mom);

        mom.turnOnTv(televison.getId());
        assertEquals(televison.getState(), State.ON);

        mom.turnOffTv(televison.getId());
        assertEquals(televison.getState(),State.OFF);
    }
    @Test
    public void bakeTest() throws InterruptedException {
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.mom = new Mother(33, "Female", "Anna", "Anička");
        Oven oven= new Oven("Xiomi",99);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom);
        EventBroker.getInstance().addToEventHandlers(mom);
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),0);
        mom.bake(oven.getId(),220,5);
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),1);
        mom.bake();
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),0);
        assertEquals(EventBroker.getInstance().getHistoryOfActivities().size(),1);
        Thread.sleep(10000);
        assertEquals(oven.getTemperature(),0);
    }
    @Test
    public void cleanBabyTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        Child kid = new Child(3,"Male","Jan","Honza");
        this.mom = new Mother(33, "Female", "Anna", "Anička");
        Father dad = new Father(33, "Male", "Pepa", "Pepíček");
        Mother mom = new Mother(25, "Female", "Josefína", "Pepíčka");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(mom);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(kid);
        EventBroker.getInstance().addToEventHandlers(dad);
        EventBroker.getInstance().addToEventHandlers(mom);
        EventBroker.getInstance().getHistoryOfEvents().clear();

        kid.makePoop();
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),1);
        assertEquals(dad.getUnhandledEvents().size(),1);
        assertEquals(mom.getUnhandledEvents().size(),1);
        assertEquals(EventBroker.getInstance().getUnhandledEvents().size(),1);

        dad.handleUnhandledEvent();
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),1);
        assertEquals(dad.getUnhandledEvents().size(),1);
        assertEquals(mom.getUnhandledEvents().size(),1);
        assertEquals(EventBroker.getInstance().getUnhandledEvents().size(),1);
        mom.handleUnhandledEvent();

        assertEquals(dad.getUnhandledEvents().size(),0);
        assertEquals(mom.getUnhandledEvents().size(),0);
        assertEquals(EventBroker.getInstance().getUnhandledEvents().size(),0);
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),3); // poop event+fix event+clean event = 3
    }
}
