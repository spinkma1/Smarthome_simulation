package CreatureTests;

import cz.cvut.fel.omo.Creatures.Dog;
import cz.cvut.fel.omo.Creatures.Father;
import cz.cvut.fel.omo.Creatures.Mother;
import cz.cvut.fel.omo.Devices.Heater;
import cz.cvut.fel.omo.Devices.TV;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.HouseBuilder;
import cz.cvut.fel.omo.House.SmartHome;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FatherTest {
    private SmartHome home;
    private Dog pes;
    private Father dad;
    @Test
    public void cleanPoopTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.pes = new Dog(5, "Male", "Bruce", "Vayne");
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(pes);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        EventBroker.getInstance().addToEventHandlers(dad);
        pes.makeAPoop();
        dad.handleUnhandledEvent();
        assertEquals(pes.getRoom().getName(), dad.getRoom().getName());
        assertEquals(dad.getUnhandledEvents().isEmpty(),true);
    }
    @Test
    public void setTemperatureToHeatTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        Heater heater= new Heater("Xiomi",120);
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(heater);
        EventBroker.getInstance().addToEventHandlers(dad);
        dad.setTemperatureToHeat(heater.getId(),999);
        assertEquals(heater.getTemperatureToHeatAt(),999);
    }
    @Test
    public void changingVolumeOfTvTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        TV televison= new TV("Xiomi",99);
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(televison);
        EventBroker.getInstance().addToEventHandlers(dad);

        dad.volumeUpTV(televison.getId());
        assertEquals(televison.getVolume(),11);

        dad.changeChannelOfTV(televison.getId(),64);
        assertEquals(televison.getChannel(),64);
    }
    @Test
    public void exerciseTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        EventBroker.getInstance().addToEventHandlers(dad);
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),0);
        dad.exercise();
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),1);
        dad.exercise();
        assertEquals(EventBroker.getInstance().getCurrentActivities().size(),0);
        assertEquals(EventBroker.getInstance().getHistoryOfActivities().size(),1);
    }
    @Test
    public void handleEventTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.pes = new Dog(5, "Male", "Bruce", "Vayne");
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        Mother mom = new Mother(25, "Female", "Josefína", "Pepíčka");
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(pes);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(mom);
        EventBroker.getInstance().addToEventHandlers(dad);
        EventBroker.getInstance().addToEventHandlers(mom);
        EventBroker.getInstance().getHistoryOfEvents().clear();

        pes.makeAPoop();
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),1);
        assertEquals(dad.getUnhandledEvents().size(),1);
        assertEquals(mom.getUnhandledEvents().size(),1);
        assertEquals(EventBroker.getInstance().getUnhandledEvents().size(),1);
        dad.handleUnhandledEvent();

        assertEquals(dad.getUnhandledEvents().size(),0);
        assertEquals(mom.getUnhandledEvents().size(),0);
        assertEquals(EventBroker.getInstance().getUnhandledEvents().size(),0);
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),3); // poop event+fix event+clean event = 3
    }
}
