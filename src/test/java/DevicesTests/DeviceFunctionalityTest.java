package DevicesTests;

import cz.cvut.fel.omo.Devices.LightBulb;
import cz.cvut.fel.omo.Devices.Sensors.MotionSensor;
import cz.cvut.fel.omo.Devices.Utils.State;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.House;
import cz.cvut.fel.omo.House.HouseBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DeviceFunctionalityTest {
    @Test
    public void testTurningOnAndOff() {
        LightBulb b = new LightBulb("", 50);
        EventBroker.getInstance().resetHistoryOfEvents();
        Assertions.assertTrue(EventBroker.getInstance().getHistoryOfEvents().isEmpty());
        b.turnOn();
        Assertions.assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),1);
        b.turnOn();
        Assertions.assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),1);
        b.turnOff();
        Assertions.assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),2);
        b.turnOff();
        Assertions.assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),2);
    }
    @Test
    public void testDevicesReaction1() throws InterruptedException {
        LightBulb bulb = new LightBulb("", 60, true);
        MotionSensor sensor = new MotionSensor("Cop 5000", 50);
        House h = new HouseBuilder().getEmptyHouse().addFloor().addRoom(1, "Living Room")
                .addDevice(sensor, "Living Room")
                .addDevice(bulb, "Living Room").build();
        Assertions.assertSame(bulb.getState(), State.OFF);
        sensor.notifyMotion();
        Assertions.assertSame(bulb.getState(), State.ON);
        Thread.sleep(10050);
        Assertions.assertSame(bulb.getState(), State.OFF);
        bulb.disableMotionHandling();
        sensor.notifyMotion();
        Assertions.assertSame(bulb.getState(), State.OFF);
        bulb.allowMotionHandling();
        sensor.notifyMotion();
        Assertions.assertSame(bulb.getState(), State.ON);
        bulb.turnOn();
        Thread.sleep(10050);
        Assertions.assertSame(bulb.getState(), State.ON);
    }
    @Test
    public void testDevicesReaction2() throws InterruptedException {
        LightBulb bulb = new LightBulb("", 60, true);
        MotionSensor sensor = new MotionSensor("Cop 5000", 50);
        House h = new HouseBuilder().getEmptyHouse().addFloor().addRoom(1, "Living Room")
                .addDevice(sensor, "Living Room")
                .addDevice(bulb, "Living Room").build();
        bulb.turnOn();
        Assertions.assertSame(bulb.getState(), State.ON);
        sensor.notifyMotion();
        Assertions.assertSame(bulb.getState(), State.ON);
        Thread.sleep(10050);
        Assertions.assertSame(bulb.getState(), State.ON);
        bulb.turnOff();
        Assertions.assertSame(bulb.getState(), State.OFF);
        sensor.notifyMotion();
        Assertions.assertSame(bulb.getState(), State.ON);
        Thread.sleep(10050);
        Assertions.assertSame(bulb.getState(), State.OFF);
    }


}
