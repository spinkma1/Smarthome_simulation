package DevicesTests;

import cz.cvut.fel.omo.Creatures.Child;
import cz.cvut.fel.omo.Creatures.Dog;
import cz.cvut.fel.omo.Creatures.Father;
import cz.cvut.fel.omo.Creatures.Mother;
import cz.cvut.fel.omo.Devices.Sensors.SmokeSensor;
import cz.cvut.fel.omo.Devices.TV;
import cz.cvut.fel.omo.Events.EventBroker;
import cz.cvut.fel.omo.House.HouseBuilder;
import cz.cvut.fel.omo.House.SmartHome;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SensorTest {
    private SmartHome home;
    private Father dad;
    @Test
    public void smokeSensorRegisterFireTest(){// sensor register fire, father evacuates himself, mother leaves with child and dog, father call 911 if everyone escped the house already. After the call, house is safe.
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        SmokeSensor sensor= new SmokeSensor("Name",2);
        this.dad = new Father(33, "Male", "Pepa", "Pepíček");
        Mother mom = new Mother(55,"Female","Jana","Janička");
        Dog dog= new Dog(556,"Male","Alfons","Alf");
        Child kid= new Child(1,"Male","Alíjk","Al");
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(dad);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(mom);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(dog);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(kid);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(sensor);
        EventBroker.getInstance().addToEventHandlers(dad);
        EventBroker.getInstance().addToEventHandlers(mom);
        mom.leaveHouse();
        assertEquals(mom.getRoom(),null);
        assertEquals(dad.getOutside(),null);
        sensor.notifySmoke();
        mom.handleUnhandledEvent();
        assertEquals(mom.getRoom(),null);
        assertEquals(kid.getRoom(),null);
        assertEquals(dog.getRoom(),null);
        dad.handleUnhandledEvent();
        assertEquals(dad.getRoom(),null);
        assertEquals(EventBroker.getInstance().getHistoryOfEvents().size(),8);
        assertEquals(dad.getUnhandledEvents().size(),0);
        assertEquals(mom.getUnhandledEvents().size(),0);
    }
}
