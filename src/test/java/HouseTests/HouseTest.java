package HouseTests;

import cz.cvut.fel.omo.Creatures.Dog;
import cz.cvut.fel.omo.Creatures.Mother;
import cz.cvut.fel.omo.Devices.Blind;
import cz.cvut.fel.omo.Devices.Oven;
import cz.cvut.fel.omo.Devices.TV;
import cz.cvut.fel.omo.House.HouseBuilder;
import cz.cvut.fel.omo.House.SmartHome;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HouseTest {
    private SmartHome home;
    private Dog pes;
    private Mother mom;
    @Test
    public void getTotalNumberTest(){
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        this.pes = new Dog(5, "Male", "Bruce", "Vayne");
        this.mom = new Mother(33, "Female", "Tereza", "Terka");
        Oven oven= new Oven("Xiomi",99);
        this.home.getHouse().getFloors().get(0).getRooms().get(0).addCreature(pes);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addCreature(mom);
        assertEquals(this.home.getHouse().getTotalNumberOfDevices(),4);//smoke sensor, blind, blind,lightbulb
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(oven);

        assertEquals(this.home.getHouse().getTotalNumberOfCreatures(),2);
        assertEquals(this.home.getHouse().getTotalNumberOfDevices(),5);
    }
    @Test
    public void getTotalConsumptionTest() {
        this.home = new SmartHome();
        home.setHouse(new HouseBuilder().getHousePrototype().build());
        Oven oven= new Oven("Xiomi",99);
        TV tv=new TV("LG",550);
        this.home.getHouse().getFloors().get(0).getRooms().get(1).addDevice(oven);
        oven.turnOn();
        oven.bake(200,5);
        tv.turnOn();
        Blind b = new Blind("Blinds");
        home.getHouse().installDevice(b,"Living room");
        for (int i = 0; i < 300; i++) {
            b.lowerBlinds();
            b.raiseBlinds();
        }
        assertEquals(this.home.getHouse().getTotalConsumption(),5);
    }
}
